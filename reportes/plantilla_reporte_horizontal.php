<?php
	error_reporting(0);
	require_once( "../php/includes/fpdf/fpdf.php" );

	class PDF extends FPDF
	{
		function Header()
		{

		    require_once('../php/config.php');
		    require_once('../php/services/ServiceReportes.php');

		    $servicio = new ServiceReportes();

		    $resultadoTienda = $servicio->getListaReporteTienda();
		    $numeroTienda = $resultadoTienda[0]->numeroTienda;
		    $nombreTienda = $resultadoTienda[0]->nombreTienda;

			$this->Image( "../img/logoReporte.png", 10, 5, 0 );
			$this->SetFont('Arial','',8);
			$this->SetFont('Arial','B',8);
			$this->Cell(20);
			$this->Cell(240, 5, "$nombreTienda - $numeroTienda", 0, 0, 'C' );
			$this->SetFont('Arial','',8);
			$this->Cell(20, 0, date("Y-m-d"), 0, 0, 'R' );
			$this->Ln(3);

			$this->Cell(20);
			$this->Cell(240);
			$this->Cell(20, 0, date("H:i:s"), 0, 0, 'R' );
			$this->Ln(7);
		}
		
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Arial','', 8);
			$this->Cell(0,10, 'Pagina '.$this->PageNo(),0,0,'C' );
		}

	}
?>