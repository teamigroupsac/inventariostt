<?php
    include("plantilla_reporte_horizontal.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-d");

    $service = new ServiceReportes();
    $estado = 1; //ACTIVO

    $data = $service->getReporteProductividadActualizado();
    $registros = $data;

    $tamanoLetra = 7;

    $reportName = "REPORTE DE PRODUCTIVIDAD ACTUALIZADO";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'L', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 270, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 5, $altoFila, 'N', $borde, 0, $alineacion);
        $pdf->Cell( 18, $altoFila, 'DNI', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, 'NOMBRES', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'REGISTROS', $borde, 0, 'R');
        $pdf->Cell( 20, $altoFila, 'UNIDADES', $borde, 0, 'R');
        $pdf->Cell( 35, $altoFila, 'FECHA INICIO', $borde, 0, $alineacion);
        $pdf->Cell( 35, $altoFila, 'FECHA FINAL', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'HORAS TOTAL', $borde, 0, 'R');
        //$pdf->Cell( 20, $altoFila, 'MIN TOTAL', $borde, 0, 'R');
        $pdf->Cell( 20, $altoFila, 'TRAB', $borde, 0, 'R');
        $pdf->Cell( 20, $altoFila, 'NO TRAB', $borde, 0, 'R');
        $pdf->Cell( 20, $altoFila, 'REG X HORA', $borde, 0, 'R');
        $pdf->Cell( 20, $altoFila, 'UNID X HORA', $borde, 0, 'R');
        $pdf->Ln($altoFila);

        $i = 0;
        $registros_total = 0.000;
        $unidades_total = 0.000;
        $minutos_total = 0.000;
        $minutos_trabajados = 0.000;
        $minutos_no_trabajados = 0.000;
        foreach ($registros as $fila) {
            $i++;
            $pdf->SetFont( 'Arial', '', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, $i, $borde, 0, $alineacion);
            $pdf->Cell( 18, $altoFila, $fila->usuario, $borde, 0, $alineacion);
            $pdf->Cell( 40, $altoFila, $fila->nombreUsuario, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->registros,3), $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, number_format($fila->unidades,3), $borde, 0, 'R');
            $pdf->Cell( 35, $altoFila, $fila->fecha_inicio, $borde, 0, $alineacion);
            $pdf->Cell( 35, $altoFila, $fila->fecha_final, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, $fila->tiempo_total, $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, $fila->minutos_trabajados, $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, $fila->minutos_no_trabajados, $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, number_format($fila->registros_por_hora,3), $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, number_format($fila->unidades_por_hora,3), $borde, 0, 'R');
            $pdf->Ln($altoFila);

            $registros_total = $registros_total + $fila->registros;
            $unidades_total = $unidades_total + $fila->unidades;
            //$minutos_total = $minutos_total + $fila->minutos_total;
            //$minutos_trabajados = $minutos_trabajados + $fila->minutos_trabajados;
            //$minutos_no_trabajados = $minutos_no_trabajados + $fila->minutos_no_trabajados;

        }
            $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, "", $borde, 0, $alineacion);
            $pdf->Cell( 18, $altoFila, "", $borde, 0, $alineacion);
            $pdf->Cell( 40, $altoFila, "TOTALES : ", $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($registros_total,3), $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, number_format($unidades_total,3), $borde, 0, 'R');
            $pdf->Cell( 35, $altoFila, "", $borde, 0, 'C');
            $pdf->Cell( 35, $altoFila, "", $borde, 0, 'C');
            $pdf->Cell( 20, $altoFila, "", $borde, 0, 'C');
            $pdf->Cell( 20, $altoFila, "", $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, "", $borde, 0, 'R');
            $pdf->Cell( 20, $altoFila, "", $borde, 0, 'C');
            $pdf->Cell( 20, $altoFila, "", $borde, 0, 'C');
    //}


  $pdf->Output( "reporte_usuario_actualizado.pdf", "I" );



?>