<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceCapturas extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getListaCapturas($dato){
		$area_cap = $dato;
        $area_cap_int = (int)$dato;
		$condicion = "";
		if ($area_cap > 0){
			$condicion = "WHERE area_cap = '$area_cap'";
		}
		$sql = "SELECT A.*, B.nombreUsuario, C.des_barra FROM captura A LEFT JOIN usuario B
                ON A.usuario = B.dniUsuario LEFT JOIN maestro C
                ON A.barra_cap = C.cod_barra $condicion
                GROUP BY id_captura
                ORDER BY A.id_captura ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("fecha","hora","cant_cap","nombreUsuario","des_barra"));

		$sqlBarras = "SELECT COUNT(DISTINCT barra_cap) cont_barra FROM captura $condicion";
		$resBarras = $this->db->get_results($sqlBarras);

		$sqlCantidad = "SELECT SUM(cant_cap) sum_cant FROM captura $condicion";
		$resCantidad = $this->db->get_results($sqlCantidad);

        $sqlAreaRango = "SELECT des_area_ran FROM area_rango WHERE area_ini_ran <= $area_cap_int AND area_fin_ran >= $area_cap_int";
        $resAreaRango = $this->db->get_var($sqlAreaRango);

		$listado = new stdClass();
        $listado->registros = $res;
        $listado->barras = $resBarras;
        $listado->cantidad = $resCantidad;
        $listado->areaRango = $resAreaRango;

		return $listado;
	}

	function getListaAreaCap(){
		$sql="SELECT area_cap FROM captura
			  GROUP BY area_cap
			  ORDER BY area_cap ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

    function deleteFormularioCaptura($data){
        $id_captura = $data->id_captura;
        $responsable = $data->responsable;

        $sql="UPDATE captura SET responsable = '$responsable' WHERE id_captura = $id_captura";
        $resEditar=$this->db->query($sql);

        $sql="DELETE FROM captura WHERE id_captura = $id_captura";
        $res=$this->db->query($sql);

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    /*function modificarMasivoFormularioCaptura($data){
    	$barra_cap = $data->barra_cap;
    	$cant_cap = $data->cant_cap;
    	$capturas = $data->capturas;
    	$usuario = $data->usuario;

    	$listacaptura = implode(",", $capturas);

        $sql="UPDATE captura SET barra_cap = '$barra_cap', cant_cap ='$cant_cap' WHERE id_captura IN ($listacaptura)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }*/

	function saveFormularioCaptura($data){
		$procedimiento = $data->procedimiento;
		$id_captura = $data->id_captura;
        $responsable = $data->responsable;
    	$barra_cap = $data->barra_cap;
    	$cant_cap = $data->cant_cap;
		$usuario = $data->usuario;

		if($procedimiento == "MODIFICAR"){
			$sql="UPDATE captura SET 
                responsable = '$responsable',
				barra_cap = '$barra_cap',
				cant_cap = $cant_cap
				WHERE id_captura = $id_captura";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

    function resolverConflictoBarras(){

        $sql = "UPDATE captura C SET C.sku_cap = ( SELECT M.sku_barra FROM maestro M WHERE M.cod_barra = C.barra_cap LIMIT 1)
                WHERE C.sku_cap =''";
        $res=$this->db->query($sql);

        $sql_consulta = "SELECT COUNT(*) AS cuenta FROM captura WHERE sku_cap = ''";
        $res_consulta = $this->db->get_var($sql_consulta);
        return $res_consulta;

    }

    function eliminarMasivoFormularioCaptura($data){
    	$usuarioModificador = $data->usuario;
    	$capturas = $data->capturas;
        $responsable = $data->responsable;

    	$listacaptura = implode(",", $capturas);

        $sql="UPDATE captura SET responsable = '$responsable' WHERE id_captura IN ($listacaptura)";
        $resEditar=$this->db->query($sql);

        $sql="DELETE FROM captura WHERE id_captura IN ($listacaptura)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }


    function eliminarAreaCapFormularioCaptura($data){
        $area_cap = $data->area_cap;
        $responsable = $data->responsable;

        $sql="UPDATE captura SET responsable = '$responsable' WHERE area_cap = $area_cap";
        $resEditar=$this->db->query($sql);

        $sql="DELETE FROM captura WHERE area_cap = '$area_cap'";
        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }



    function getDescargaReporteCapturaDetallada($dato){
        $hora = date("h:i:s");
        $fecha = date("Y-m-j");
        $area_cap = $dato;
        $condicion = "";

        if ($area_cap > 0){
            $condicion = "WHERE area_cap IN ($dato)";
        }

        if (strpos($dato, '-') !== false) {
            $inicio = substr($dato,0,strpos($dato, '-'));
            $final = substr($dato,strpos($dato, '-')+1,10);
            $condicion = "WHERE area_cap BETWEEN $inicio AND $final";
        }else{
            $condicion = "WHERE area_cap IN ($dato)";
        }

        $sqlRegistros = "SELECT A.*, B.nombreUsuario, C.des_barra, C.Prec_barra,ROUND((A.cant_cap * C.Prec_barra),3) AS monto FROM captura A LEFT JOIN usuario B
                ON A.usuario = B.dniUsuario LEFT JOIN maestro C
                ON A.barra_cap = C.cod_barra $condicion
                ORDER BY A.id_captura ASC";
        $resRegistros = $this->db->get_results($sqlRegistros);
        $this->_codificarObjeto($resRegistros,array("fecha","hora","cant_cap","nombreUsuario","des_barra"));

        $sqlAreas = "SELECT area_cap, SUM(cant_cap) sum_cant FROM captura 
                    $condicion
                    GROUP BY area_cap
                    ORDER BY area_cap ASC";
        $resAreas = $this->db->get_results($sqlAreas);



        if($resRegistros){
                            
            date_default_timezone_set('America/Mexico_City');

            if (PHP_SAPI == 'cli')
                die('Este archivo solo se puede ver desde un navegador web');

            /** Se agrega la libreria PHPExcel */
            //require_once 'lib/PHPExcel/PHPExcel.php';

            // Se crea el objeto PHPExcel
            $objPHPExcel = new PHPExcel();

            // Se asignan las propiedades del libro
            $objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
                                 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
                                 ->setTitle("Reporte Excel con PHP y MySQL")
                                 ->setSubject("Reporte Excel con PHP y MySQL")
                                 ->setDescription("Reporte de alumnos")
                                 ->setKeywords("reporte alumnos carreras")
                                 ->setCategory("Reporte excel");


            $tituloReporte = "REPORTE DETALLADO DE CAPTURAS - LOTE CAPTURA : ".$area_cap;
            $titulosColumnas = array('N','LOTE_CAP','BARRA_CAP','DES_BARRA','CANT_CAP','PREC_BARRA','MONTO','TIP_CAP','USUARIO','FECHA','HORA','DESCARGADO','NAMEPALM');
            
            $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A1:M1');
                            
            // Se agregan los titulos del reporte
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1',$tituloReporte)
                        ->setCellValue('G1','FECHA : ')
                        ->setCellValue('H1',$fecha)
                        ->setCellValue('G2','HORA : ')
                        ->setCellValue('H2',$hora)
                        ->setCellValue('A3',  $titulosColumnas[0])
                        ->setCellValue('B3',  $titulosColumnas[1])
                        ->setCellValue('C3',  $titulosColumnas[2])
                        ->setCellValue('D3',  $titulosColumnas[3])
                        ->setCellValue('E3',  $titulosColumnas[4])
                        ->setCellValue('F3',  $titulosColumnas[5])
                        ->setCellValue('G3',  $titulosColumnas[6])
                        ->setCellValue('H3',  $titulosColumnas[7])
                        ->setCellValue('I3',  $titulosColumnas[8])
                        ->setCellValue('J3',  $titulosColumnas[9])
                        ->setCellValue('K3',  $titulosColumnas[10])
                        ->setCellValue('L3',  $titulosColumnas[11])
                        ->setCellValue('M3',  $titulosColumnas[12]);
            
            //Se agregan los datos de los alumnos
            $i = 4;



                for( $x = 0; $x < count($resRegistros); $x++)
                {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i,  ($x + 1))
                            ->setCellValue('B'.$i,  $resRegistros[$x]->area_cap)
                            ->setCellValue('C'.$i,  $resRegistros[$x]->barra_cap)
                            ->setCellValue('D'.$i,  $resRegistros[$x]->des_barra)
                            ->setCellValue('E'.$i,  number_format($resRegistros[$x]->cant_cap,3))
                            ->setCellValue('F'.$i,  number_format($resRegistros[$x]->Prec_barra,3))
                            ->setCellValue('G'.$i,  number_format($resRegistros[$x]->monto,3))
                            ->setCellValue('H'.$i,  $resRegistros[$x]->tip_cap)
                            ->setCellValue('I'.$i,  $resRegistros[$x]->usuario)
                            ->setCellValue('J'.$i,  $resRegistros[$x]->fecha)
                            ->setCellValue('K'.$i,  $resRegistros[$x]->hora)
                            ->setCellValue('L'.$i,  $resRegistros[$x]->descargado)
                            ->setCellValue('M'.$i,  $resRegistros[$x]->namepalm);
                            $i++;
                }

            

            $estiloTituloReporte = array(
                'font' => array(
                    'name'      => 'Verdana',
                    'bold'      => true,
                    'italic'    => false,
                    'strike'    => false,
                    'size' =>16,
                        'color'     => array(
                            'rgb' => 'FFFFFF'
                        )
                ),
                'fill' => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FF220835')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE                    
                    )
                ), 
                'alignment' =>  array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        'rotation'   => 0,
                        'wrap'          => TRUE
                )
            );

            $estiloTituloColumnas = array(
                'font' => array(
                    'name'      => 'Arial',
                    'bold'      => true,                          
                    'color'     => array(
                        'rgb' => 'FFFFFF'
                    )
                ),
                'fill'  => array(
                    'type'      => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                    'rotation'   => 90,
                    'startcolor' => array(
                        'rgb' => 'c47cf2'
                    ),
                    'endcolor'   => array(
                        'argb' => 'FF431a5d'
                    )
                ),
                'borders' => array(
                    'top'     => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                        'color' => array(
                            'rgb' => '143860'
                        )
                    ),
                    'bottom'     => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                        'color' => array(
                            'rgb' => '143860'
                        )
                    )
                ),
                'alignment' =>  array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        'wrap'          => TRUE
                ));
                
            $estiloInformacion = new PHPExcel_Style();
            $estiloInformacion->applyFromArray(
                array(
                    'font' => array(
                    'name'      => 'Arial',               
                    'color'     => array(
                        'rgb' => '000000'
                    )
                ),
                'fill'  => array(
                    'type'      => PHPExcel_Style_Fill::FILL_SOLID,
                    'color'     => array('argb' => 'FFd9b7f4')
                ),
                'borders' => array(
                    'left'     => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN ,
                        'color' => array(
                            'rgb' => '3a2a47'
                        )
                    )             
                )
            ));
             
            //$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
            //$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);     
            //$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
                    
            for($i = 'A'; $i <= 'K'; $i++){
                $objPHPExcel->setActiveSheetIndex(0)            
                    ->getColumnDimension($i)->setAutoSize(TRUE);
            }
            
            // Se asigna el nombre a la hoja
            $objPHPExcel->getActiveSheet()->setTitle('Reporte');

            // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
            $objPHPExcel->setActiveSheetIndex(0);
            // Inmovilizar paneles 
            //$objPHPExcel->getActiveSheet(0)->freezePane('A4');
            $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

            // Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="reportedeCapturaDetallada.xlsx"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('reportes/reportedeCapturaDetallada.xlsx');

            return "ok";
            
        }
        else{
            print_r('No hay resultados para mostrar');
        }
    }

    function getDescargaReporteCapturaConsolidada($dato){
        $hora = date("h:i:s");
        $fecha = date("Y-m-j");
        $area_cap = $dato;
        $condicion = "";

        if ($area_cap > 0){
            $condicion = "WHERE area_cap IN ($dato)";
        }

        if (strpos($dato, '-') !== false) {
            $inicio = substr($dato,0,strpos($dato, '-'));
            $final = substr($dato,strpos($dato, '-')+1,10);
            $condicion = "WHERE area_cap BETWEEN $inicio AND $final";
        }else{
            $condicion = "WHERE area_cap IN ($dato)";
        }
        
        $sqlRegistros = "SELECT A.*, SUM(A.cant_cap) sum_cant, B.nombreUsuario, C.des_barra, C.Prec_barra,ROUND((A.cant_cap * C.Prec_barra),3) AS monto FROM captura A LEFT JOIN usuario B
                ON A.usuario = B.dniUsuario LEFT JOIN maestro C
                ON A.barra_cap = C.cod_barra $condicion
                GROUP BY A.area_cap, A.barra_cap
                ORDER BY A.id_captura ASC";
        $resRegistros = $this->db->get_results($sqlRegistros);
        $this->_codificarObjeto($resRegistros,array("fecha","hora","cant_cap","nombreUsuario","des_barra"));

        $sqlAreas = "SELECT area_cap, SUM(cant_cap) sum_cant FROM captura 
                    $condicion
                    GROUP BY area_cap
                    ORDER BY area_cap ASC";
        $resAreas = $this->db->get_results($sqlAreas);



        if($resRegistros){
                            
            date_default_timezone_set('America/Mexico_City');

            if (PHP_SAPI == 'cli')
                die('Este archivo solo se puede ver desde un navegador web');

            /** Se agrega la libreria PHPExcel */
            //require_once 'lib/PHPExcel/PHPExcel.php';

            // Se crea el objeto PHPExcel
            $objPHPExcel = new PHPExcel();

            // Se asignan las propiedades del libro
            $objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
                                 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
                                 ->setTitle("Reporte Excel con PHP y MySQL")
                                 ->setSubject("Reporte Excel con PHP y MySQL")
                                 ->setDescription("Reporte de alumnos")
                                 ->setKeywords("reporte alumnos carreras")
                                 ->setCategory("Reporte excel");


            $tituloReporte = "REPORTE CONSOLIDADO DE CAPTURAS - AREA CAPTURA : ".$area_cap;
            $titulosColumnas = array('N','AREA_CAP','BARRA_CAP','DES_BARRA','SUM_CAP','PREC_BARRA','MONTO','TIP_CAP','USUARIO','FECHA','HORA','DESCARGADO','NAMEPALM');
            
            $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A1:M1');
                            
            // Se agregan los titulos del reporte
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1',$tituloReporte)
                        ->setCellValue('G1','FECHA : ')
                        ->setCellValue('H1',$fecha)
                        ->setCellValue('G2','HORA : ')
                        ->setCellValue('H2',$hora)
                        ->setCellValue('A3',  $titulosColumnas[0])
                        ->setCellValue('B3',  $titulosColumnas[1])
                        ->setCellValue('C3',  $titulosColumnas[2])
                        ->setCellValue('D3',  $titulosColumnas[3])
                        ->setCellValue('E3',  $titulosColumnas[4])
                        ->setCellValue('F3',  $titulosColumnas[5])
                        ->setCellValue('G3',  $titulosColumnas[6])
                        ->setCellValue('H3',  $titulosColumnas[7])
                        ->setCellValue('I3',  $titulosColumnas[8])
                        ->setCellValue('J3',  $titulosColumnas[9])
                        ->setCellValue('K3',  $titulosColumnas[10])
                        ->setCellValue('L3',  $titulosColumnas[11])
                        ->setCellValue('M3',  $titulosColumnas[12]);
            
            //Se agregan los datos de los alumnos
            $i = 4;



                for( $x = 0; $x < count($resRegistros); $x++)
                {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i,  ($x + 1))
                            ->setCellValue('B'.$i,  $resRegistros[$x]->area_cap)
                            ->setCellValue('C'.$i,  $resRegistros[$x]->barra_cap)
                            ->setCellValue('D'.$i,  $resRegistros[$x]->des_barra)
                            ->setCellValue('E'.$i,  number_format($resRegistros[$x]->sum_cant,3))
                            ->setCellValue('F'.$i,  number_format($resRegistros[$x]->Prec_barra,3))
                            ->setCellValue('G'.$i,  number_format($resRegistros[$x]->monto,3))
                            ->setCellValue('H'.$i,  $resRegistros[$x]->tip_cap)
                            ->setCellValue('I'.$i,  $resRegistros[$x]->usuario)
                            ->setCellValue('J'.$i,  $resRegistros[$x]->fecha)
                            ->setCellValue('K'.$i,  $resRegistros[$x]->hora)
                            ->setCellValue('L'.$i,  $resRegistros[$x]->descargado)
                            ->setCellValue('M'.$i,  $resRegistros[$x]->namepalm);
                            $i++;
                }

            

            $estiloTituloReporte = array(
                'font' => array(
                    'name'      => 'Verdana',
                    'bold'      => true,
                    'italic'    => false,
                    'strike'    => false,
                    'size' =>16,
                        'color'     => array(
                            'rgb' => 'FFFFFF'
                        )
                ),
                'fill' => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FF220835')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE                    
                    )
                ), 
                'alignment' =>  array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        'rotation'   => 0,
                        'wrap'          => TRUE
                )
            );

            $estiloTituloColumnas = array(
                'font' => array(
                    'name'      => 'Arial',
                    'bold'      => true,                          
                    'color'     => array(
                        'rgb' => 'FFFFFF'
                    )
                ),
                'fill'  => array(
                    'type'      => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                    'rotation'   => 90,
                    'startcolor' => array(
                        'rgb' => 'c47cf2'
                    ),
                    'endcolor'   => array(
                        'argb' => 'FF431a5d'
                    )
                ),
                'borders' => array(
                    'top'     => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                        'color' => array(
                            'rgb' => '143860'
                        )
                    ),
                    'bottom'     => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                        'color' => array(
                            'rgb' => '143860'
                        )
                    )
                ),
                'alignment' =>  array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        'wrap'          => TRUE
                ));
                
            $estiloInformacion = new PHPExcel_Style();
            $estiloInformacion->applyFromArray(
                array(
                    'font' => array(
                    'name'      => 'Arial',               
                    'color'     => array(
                        'rgb' => '000000'
                    )
                ),
                'fill'  => array(
                    'type'      => PHPExcel_Style_Fill::FILL_SOLID,
                    'color'     => array('argb' => 'FFd9b7f4')
                ),
                'borders' => array(
                    'left'     => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN ,
                        'color' => array(
                            'rgb' => '3a2a47'
                        )
                    )             
                )
            ));
             
            //$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
            //$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);     
            //$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
                    
            for($i = 'A'; $i <= 'K'; $i++){
                $objPHPExcel->setActiveSheetIndex(0)            
                    ->getColumnDimension($i)->setAutoSize(TRUE);
            }
            
            // Se asigna el nombre a la hoja
            $objPHPExcel->getActiveSheet()->setTitle('Reporte');

            // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
            $objPHPExcel->setActiveSheetIndex(0);
            // Inmovilizar paneles 
            //$objPHPExcel->getActiveSheet(0)->freezePane('A4');
            $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

            // Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="reportedeCapturaConsolidada.xlsx"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('reportes/reportedeCapturaConsolidada.xlsx');

            return "ok";
            
        }
        else{
            print_r('No hay resultados para mostrar');
        }
    }

}	
?>