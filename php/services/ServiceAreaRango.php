<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceAreaRango extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

	function getNumeroTienda(){
		$sql = "SELECT * FROM tienda ORDER BY idTienda ASC LIMIT 1";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombreTienda"));
		return $res;
	}

	function getListaAreaRango(){
		$sqlAvance = "	SELECT a.idAreaRango, a.area_ini_ran, a.area_fin_ran, a.des_area_ran, a.fecha, a.idUsuario, a.ubicacion, ((area_fin_ran - area_ini_ran)+1) cantidad,COUNT(DISTINCT b.lote) avance, ROUND(((COUNT(DISTINCT b.lote) / ((area_fin_ran - area_ini_ran)+1))*100),2) porcentaje, GROUP_CONCAT(DISTINCT c.lote SEPARATOR ',') lotes
						FROM area_rango a LEFT JOIN
						(
						SELECT DISTINCT area_cap AS lote FROM captura
						UNION DISTINCT
						SELECT lote FROM justificacion
						) b
						ON b.lote BETWEEN a.area_ini_ran AND a.area_fin_ran LEFT JOIN justificacion c
						ON c.lote BETWEEN a.area_ini_ran AND a.area_fin_ran 
						GROUP BY a.idAreaRango ORDER BY a.area_ini_ran ASC";
		$resAvance = $this->db->get_results($sqlAvance);
		$this->_codificarObjeto($resAvance,array("des_area_ran"));

		return $resAvance;
	}

	function getLotesNoValidos(){
		$sql = "SELECT c.area_cap FROM area_rango a RIGHT JOIN 
				(
				SELECT c.area_cap FROM captura c GROUP BY c.area_cap
				) c
				ON c.area_cap BETWEEN a.area_ini_ran AND a.area_fin_ran
				WHERE a.des_area_ran IS NULL";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getRangoCaptura($inicio,$final){
		$sql = "SELECT area_cap FROM captura c WHERE c.area_cap BETWEEN $inicio AND $final GROUP BY c.area_cap ORDER BY c.area_cap ASC";
		$res = $this->db->get_results($sql);

		$sqlJusti = "SELECT * FROM justificacion";
		$resJusti = $this->db->get_results($sqlJusti);

        $resultado = new stdClass();
        $resultado->regis = $res;
        $resultado->justi = $resJusti;

		return $resultado;
	}

	function saveFormularioAreaRango($data){
		$procedimiento = $data->procedimiento;
		$idAreaRango = $data->idAreaRango;
		$inicio = $data->inicio;
		$final = $data->final;
		$descripcion = $data->descripcion;
		$ubicacion = $data->ubicacion;
		$usuario = $data->usuario;
		$fecha = $data->fecha;

		if($procedimiento == "GUARDAR"){
			$sql="INSERT INTO area_rango (idUsuario,des_area_ran,ubicacion,area_ini_ran,area_fin_ran,fecha)
				VALUES ('$usuario',UPPER('$descripcion'),'$ubicacion',$inicio,$final,'$fecha')";

			$resNuevo=$this->db->query($sql);
		}else if($procedimiento == "MODIFICAR"){
			$sql="UPDATE area_rango SET 
				des_area_ran = UPPER('$descripcion'),
				ubicacion = '$ubicacion',
				area_ini_ran = $inicio,
				area_fin_ran = $final
				WHERE idAreaRango = $idAreaRango";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

    function deleteFormularioAreaRango($dato){
        $sql="DELETE FROM area_rango WHERE idAreaRango = $dato";
        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    function eliminarMasivoFormularioAreaRango($data){
    	$usuarioRegistrador = $data->usuario;
    	$areaRango = $data->areaRango;

    	$listaarearango = implode(",", $areaRango);

        $sql="DELETE FROM area_rango WHERE idAreaRango IN ($listaarearango)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }


    function saveJustificacionLoteAreaRango($data){
    	$lote = $data->lote;
    	$ubicacion = $data->ubicacion;
    	$tipo = $data->tipo;
    	$justificacion = $data->justificacion;
    	$procedimiento = $data->procedimiento;

		if($procedimiento == "GUARDAR"){
			$sql="INSERT INTO justificacion (lote,ubicacion,tipo,justificacion)
				VALUES ($lote,'$ubicacion','$tipo','$justificacion')";

			$resNuevo=$this->db->query($sql);
		}else if($procedimiento == "MODIFICAR"){
			$sql="UPDATE justificacion SET 
				ubicacion = '$ubicacion',
				tipo = '$tipo',
				justificacion = '$justificacion'
				WHERE lote = $lote";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

    }

    function getListaJustificacionAreaRango(){
    	$sql = "SELECT * FROM justificacion";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("ubicacion","justificacion"));
    	return $res;
    }

	function getListaAreaRangoNoValidos($dato){
		//$area_cap = "'".implode("','", $dato)."'";
		$area_cap = 0;
		for ($i=0; $i < count($dato); $i++) { 
			$area_cap .= ','.$dato[$i]->area_cap;
		}
		$sql = "SELECT area_cap, SUM(cant_cap) AS sum_cant FROM captura
				WHERE area_cap IN ($area_cap)
				GROUP BY area_cap
				ORDER BY area_cap ASC";
		$res = $this->db->get_results($sql);

		return $res;
	}

	function getDescargaGraficoAvanceCaptura(){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");


		$sqlRangos = "SELECT * FROM area_rango ORDER BY area_ini_ran ASC";
		$resRangos = $this->db->get_results($sqlRangos);
		$this->_codificarObjeto($resRangos,array("des_area_ran"));

		$sqlCaptura = "SELECT DISTINCT area_cap FROM captura ORDER BY area_cap ASC";
		$resCaptura = $this->db->get_results($sqlCaptura);

		$sqlJustificados = "SELECT * FROM justificacion
							WHERE lote NOT IN (SELECT DISTINCT area_cap FROM captura)";
		$resJustificados = $this->db->get_results($sqlJustificados);
		$this->_codificarObjeto($resJustificados,array("tipo","justificacion"));


		$dataRangos = $resRangos;
	    $dataCapturas = $resCaptura;
	    $dataJustificados = $resJustificados;

	    $cuentaRangos = count($dataRangos);

		if($dataRangos){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			// Se asignan las propiedades del libro
			$objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
								 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
								 ->setTitle("Reporte Excel con PHP y MySQL")
								 ->setSubject("Reporte Excel con PHP y MySQL")
								 ->setDescription("Reporte de alumnos")
								 ->setKeywords("reporte alumnos carreras")
								 ->setCategory("Reporte excel");

			$tituloReporte = "REPORTE AREA RANGO";
			$titulosColumnas = array('N','INICIO','FINAL','DESCRIPCION','CANTIDAD','AVANCE','PORCENTAJE');
			
			$objPHPExcel->setActiveSheetIndex(0)
	        		    ->mergeCells('A1:H1');
							
			// Se agregan los titulos del reporte
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1',$tituloReporte)
						->setCellValue('G1','FECHA : ')
						->setCellValue('H1',$fecha)
						->setCellValue('G2','HORA : ')
						->setCellValue('H2',$hora)
	        		    ->setCellValue('A3',  $titulosColumnas[0])
			            ->setCellValue('B3',  $titulosColumnas[1])
	        		    ->setCellValue('C3',  $titulosColumnas[2])
	            		->setCellValue('D3',  $titulosColumnas[3])
	            		->setCellValue('E3',  $titulosColumnas[4])
	            		->setCellValue('F3',  $titulosColumnas[5])
	            		->setCellValue('G3',  $titulosColumnas[6]);
			
			//Se agregan los datos de los alumnos
			$f = 4;


        for($i=0 ; $i < $cuentaRangos ; $i++){

            $filaInicio = $dataRangos[$i]->area_ini_ran;
            $filaFinal = $dataRangos[$i]->area_fin_ran;
            $filaDescripcion = $dataRangos[$i]->des_area_ran;
            $filaUsuario = $dataRangos[$i]->idUsuario;
            $cantidad = ( (int)$filaFinal - (int)$filaInicio ) + 1;
            $avance = 0;
            $porcentaje = 0;

            for ($z=$filaInicio; $z<=$filaFinal; $z++) { 
                for ($y=0; $y<count($dataCapturas); $y++) {
                    $valorArea = (int)$dataCapturas[$y]->area_cap;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }

                for ($x=0; $x<count($dataJustificados); $x++) {
                    $valorArea = (int)$dataJustificados[$x]->lote;
                    if ($z == $valorArea){
                        $avance++;
                        $loteJusti[]= array('lote' => $dataJustificados[$x]->lote, 'justificacion' => $dataJustificados[$x]->justificacion);
                    }
                }


            }

            $porcentaje = round(($avance / $cantidad)*100,2);

			$objPHPExcel->setActiveSheetIndex(0)
	        			->setCellValue('A'.$f,  ($x + 1))
						->setCellValue('B'.$f,  $filaInicio)
	        			->setCellValue('C'.$f,  $filaFinal)
	        			->setCellValue('D'.$f,  $filaDescripcion)
	        			->setCellValue('E'.$f,  $cantidad)
	        			->setCellValue('F'.$f,  $avance)
	        			->setCellValue('G'.$f,  $porcentaje);
					$f++;

        }




			
			$estiloTituloReporte = array(
	        	'font' => array(
		        	'name'      => 'Verdana',
	    	        'bold'      => true,
	        	    'italic'    => false,
	                'strike'    => false,
	               	'size' =>16,
		            	'color'     => array(
	    	            	'rgb' => 'FFFFFF'
	        	       	)
	            ),
		        'fill' => array(
					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'	=> array('argb' => 'FF220835')
				),
	            'borders' => array(
	               	'allborders' => array(
	                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	               	)
	            ), 
	            'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'rotation'   => 0,
	        			'wrap'          => TRUE
	    		)
	        );

			$estiloTituloColumnas = array(
	            'font' => array(
	                'name'      => 'Arial',
	                'bold'      => true,                          
	                'color'     => array(
	                    'rgb' => 'FFFFFF'
	                )
	            ),
	            'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation'   => 90,
	        		'startcolor' => array(
	            		'rgb' => 'c47cf2'
	        		),
	        		'endcolor'   => array(
	            		'argb' => 'FF431a5d'
	        		)
				),
	            'borders' => array(
	            	'top'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                ),
	                'bottom'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                )
	            ),
				'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'wrap'          => TRUE
	    		));
				
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray(
				array(
	           		'font' => array(
	               	'name'      => 'Arial',               
	               	'color'     => array(
	                   	'rgb' => '000000'
	               	)
	           	),
	           	'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFd9b7f4')
				),
	           	'borders' => array(
	               	'left'     => array(
	                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
		                'color' => array(
	    	            	'rgb' => '3a2a47'
	                   	)
	               	)             
	           	)
	        ));
			 
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);		
			//$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
					
			for($i = 'A'; $i <= 'H'; $i++){
				$objPHPExcel->setActiveSheetIndex(0)			
					->getColumnDimension($i)->setAutoSize(TRUE);
			}
			
			// Se asigna el nombre a la hoja
			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
			$objPHPExcel->setActiveSheetIndex(0);
			// Inmovilizar paneles 
			//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
			$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reportegraficoarearango.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/reportegraficoarearango.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}

}	
?>