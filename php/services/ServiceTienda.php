<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceTienda extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getListaTienda(){
		$sql = "SELECT A.*, B.nombreUsuario FROM tienda A LEFT JOIN usuario B
				ON A.cliente = B.dniUsuario
				ORDER BY A.idTienda ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombreTienda","nombreUsuario"));
		return $res;
	}

	function getListaClientes(){
		$sql="	SELECT dniUsuario, nombreUsuario FROM usuario
				WHERE tipoUsuario = 4
				ORDER BY nombreUsuario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombreUsuario"));
		return $res;
	}

	function saveFormularioTienda($data){
		$procedimiento = $data->procedimiento;
		$idTienda = $data->idTienda;
		$numeroTienda = $data->numeroTienda;
		$nombreTienda = $data->nombreTienda;
		$nombreUsuario = $data->nombreUsuario;
		$fechaTienda = $data->fechaTienda;
		$usuario = $data->usuario;

		if($procedimiento == "GUARDAR"){
			$sql="INSERT INTO tienda (numeroTienda,nombreTienda,cliente,fechaTienda)
				VALUES ('$numeroTienda',UPPER('$nombreTienda'),'$nombreUsuario','$fechaTienda')";

			$resNuevo=$this->db->query($sql);
		}else if($procedimiento == "MODIFICAR"){
			$sql="UPDATE tienda SET 
				numeroTienda = '$numeroTienda',
				nombreTienda = UPPER('$nombreTienda'),
				cliente = '$nombreUsuario',
				fechaTienda = '$fechaTienda'
				WHERE idTienda = $idTienda";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

    function deleteFormularioTienda($dato){
        $sql="DELETE FROM tienda WHERE idTienda= $dato";
        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    function eliminarMasivoFormularioTienda($data){
    	$usuarioRegistrador = $data->usuario;
    	$tiendas = $data->tiendas;

    	$listatienda = implode(",", $tiendas);

        $sql="DELETE FROM tienda WHERE idTienda IN ($listatienda)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }










}	
?>