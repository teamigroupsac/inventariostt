<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceArchivosPendientes extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

	

	function listarArchivosPendientes(){
		/*$sql="	UPDATE captura SET cant_cap = (cant_cap / 1000)
				WHERE tip_cap = 4";
		$res=$this->db->query($sql);*/
		
		$archivos = array();

		$directorio = opendir(FILE_PENDING);//opendir("../archivos_sistema/archivos_pendientes"); //ruta actual
		while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
		{
		    if (is_dir($archivo))//verificamos si es o no un directorio
		    {
		        //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
		    }
		    else
		    {
		    	if(substr($archivo,-4)==".txt"){
			    	$file = new stdClass();
			        $file->nombre = $archivo;
			        //$file->peso = filesize("../archivos_sistema/archivos_pendientes/".$archivo)." KB";
			        $file->peso = filesize(FILE_PENDING.'/'.$archivo)." KB";
			        //$file->fecha = date("Y-m-d", filectime("../archivos_sistema/archivos_pendientes/".$archivo));
			        $file->fecha = date("Y-m-d", filectime(FILE_PENDING.'/'.$archivo));

			        $archivos[] = $file;
		    	}
		    }

		    
		}

		return $archivos;

	}

	function cargarArchivoPendienteMasivo($dato){
		$archivo = FILE_PENDING.'/'.$dato;
		$sql = "LOAD DATA LOCAL INFILE '".$archivo."' INTO TABLE captura FIELDS TERMINATED BY '	' LINES TERMINATED BY '\n'";
		$res=$this->db->query($sql);

		copy(FILE_PENDING.'/'.$dato,"../archivos_sistema/archivos_procesados/".$dato);
		unlink(FILE_PENDING.'/'.$dato);
		
		$proceso = new stdClass();
		$proceso->estado = $res;
		$proceso->nombre = $dato;

		return $proceso;
	}

	function cargarArchivoPendiente($dato){
		$archivo = FILE_PENDING.'/'.$dato;
		$sql = "LOAD DATA LOCAL INFILE '".$archivo."' INTO TABLE captura FIELDS TERMINATED BY '	' LINES TERMINATED BY '\n'";
		$res=$this->db->query($sql);

		$proceso = new stdClass();
		$proceso->estado = $res;
		$proceso->nombre = $dato;

		return $proceso;
	}

	function copiarArchivoPendiente($dato){
		$sql="	UPDATE captura SET cant_cap = (cant_cap / 1000)
				WHERE tip_cap = 4";
		$res=$this->db->query($sql);

		copy(FILE_PENDING.'/'.$dato,"../archivos_sistema/archivos_procesados/".$dato);
		unlink(FILE_PENDING.'/'.$dato);

		$total_archivos = count(glob(FILE_PENDING.'/'.'{*.txt}',GLOB_BRACE));
		return $total_archivos;

	}

	function eliminarArchivoPendiente($dato){
		copy(FILE_PENDING.'/'.$dato,"../archivos_sistema/archivos_eliminados/".$dato);
		unlink(FILE_PENDING.'/'.$dato);

		$total_archivos = count(glob(FILE_PENDING.'/'.'{*.txt}',GLOB_BRACE));
		return $total_archivos;

	}

	function backupBaseDatosCaptura(){
	    //connect & select the database
	    //$dbHost,$dbUsername,$dbPassword,$dbName,$tables,$directorio
	    $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME); 

	    $return = "";
	    //get all of the tables
	    $tables = DB_BACKUP_TABLES;
	    if($tables == '*'){
	        $tables = array();
	        $result = $db->query("SHOW TABLES");
	        while($row = $result->fetch_row()){
	            $tables[] = $row[0];
	        }
	    }else{
	        $tables = is_array($tables)?$tables:explode(',',$tables);
	    }

	    //loop through the tables
	    foreach($tables as $table){
	        $result = $db->query("SELECT * FROM $table");
	        $numColumns = $result->field_count;

	        $return .= "DROP TABLE $table;";

	        $result2 = $db->query("SHOW CREATE TABLE $table");
	        $row2 = $result2->fetch_row();

	        $return .= "\n\n".$row2[1].";\n\n";

	        for($i = 0; $i < $numColumns; $i++){
	            while($row = $result->fetch_row()){
	                $return .= "INSERT INTO $table VALUES(";
	                for($j=0; $j < $numColumns; $j++){
	                    $row[$j] = addslashes($row[$j]); //$row[$j];
	                    $row[$j] = str_replace("'"," ",$row[$j]);
	                    $row[$j] = str_replace('"','´´',$row[$j]);
	                    $row[$j] = str_replace("\n"," ",$row[$j]);
	                    if (isset($row[$j])) { $return .= '"'.$row[$j].'"' ; } else { $return .= '""'; }
	                    if ($j < ($numColumns-1)) { $return.= ','; }
	                }
	                $return .= ");\n";
	            }
	        }

	        $return .= "\n\n\n";
	    }

	    $numero = $this->getDato("numeroTienda","tienda","idTienda > 0 LIMIT 1");
	    $nombre = $this->getDato("nombreTienda","tienda","idTienda > 0 LIMIT 1");
	    $fecha = date("Y-m-d");
	    $hora = date("H-i-s");

	    //save file
	    $directorio = DB_BACKUP_DEVICE_USB;
	    $directorio_local = DB_BACKUP_DEVICE_LOCAL;

	    $existe_usb = $this->comprobarDispositivoPendientes();
	    if($existe_usb == 1){
	    	$handle = fopen($directorio.':\backup-'.$numero.'-'.$nombre.'-'.$fecha.'--'.$hora.'.sql','w+');
	    }else{
	    	$handle = fopen($directorio_local.':\backup-'.$numero.'-'.$nombre.'-'.$fecha.'--'.$hora.'.sql','w+');
	    }

	    fwrite($handle,$return);
	    fclose($handle);

	    return "OK";
	}

	function comprobarDispositivoPendientes(){
		$directorio = DB_BACKUP_DEVICE_USB;
		$nombre_fichero = $directorio.":\backup.txt";
		$retorno = 0;

		if (file_exists($nombre_fichero)) {
		    $retorno = 1;
		} else {
		    $retorno = 0;
		}

		return $retorno;

	}

}	
?>