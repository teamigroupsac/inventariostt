
<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceGenerarArchivos extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function generarArchivoUsuario($dato){
		$condicion = "";
		if($dato != ""){
			$condicion = "WHERE estadoUsuario = $dato AND dniUsuario NOT IN (10355653,41479174,41467062) AND tipoUsuario != 4";
		}

		$sql = "SELECT dniUsuario,claveUsuario,estadoUsuario,tipoUsuario FROM usuario $condicion";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/usuario.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->dniUsuario."|".$res[$i]->claveUsuario."|".$res[$i]->estadoUsuario; //."|".$res[$i]->tipoUsuario;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;
        copy("../archivos_sistema/archivos_generados/usuario.txt","../../proyecto/carga/usuario.txt");
		return $archivos;

	}

	function generarArchivoMaestro(){

		$sql = "SELECT DISTINCT cod_barra FROM maestro";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;
        copy("../archivos_sistema/archivos_generados/maestro.txt","../../proyecto/carga/maestro.txt");
		return $archivos;

	}

	function generarArchivoMaestroDetalle(){

		$sql = "SELECT cod_barra, des_barra FROM maestro GROUP BY cod_barra";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro_detalle.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra."|".$res[$i]->des_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}


	function comprobarInformacionArchivoGen($file){
		$archivo = "../archivos_sistema/archivos_generados/".$file.".txt";
		$bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );
    	$conteo = count(file($archivo));

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = ($conteo - 1);
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarprimerarchivo(){
			$sql = "UPDATE captura_cargaif SET tip_cap = 1 WHERE tip_cap != 5";
		$res=$this->db->query($sql);

		$sql="	SELECT CAST(M.nro_cont_mst AS UNSIGNED) nro_cont_mst,M.loc_mst,C.sku_cap sku_cap, RIGHT(CONCAT('0000000000000',C.barra_cap),13) barra_cap,	IF(c.tip_cap = 5,'05','01') tip_cap,c.cant_cap cant_cap,CONCAT(SUBSTRING(M.fec_cong_mst, 7,4),SUBSTRING(M.fec_cong_mst, 4,2),SUBSTRING(M.fec_cong_mst, 1,2)) fec_cong_mst
				FROM maestro M RIGHT JOIN 
				(
				SELECT SUM(cant_cap) cant_cap, barra_cap,tip_cap, sku_cap FROM captura_cargaif
				GROUP BY sku_cap, barra_cap, tip_cap
				) C
				ON M.sku_barra = C.sku_cap
				GROUP BY C.sku_cap,C.barra_cap,C.tip_cap ";
		$res = $this->db->get_results($sql);
		$archivo = "../archivos_sistema/archivos_generados/IF_CARGA.txt";
		unlink('$archivo');
		//$cadena="\r\n";
		$conteo=0;
		//$cadena.="\r\n";
		for($i=0;$i<count($res);$i++){
			$n_cant_cap == "-";
			$n_simbolo = "";
			$cant_cap = $res[$i]->cant_cap;
			if($cant_cap < 0){
				$n_cant_cap = SUBSTR("00000000000000000".($cant_cap * (-1)),-17);
				$n_simbolo = "-";
			}else{
				$n_cant_cap = SUBSTR("000000000000000000".$cant_cap,-18);
			}
			$conteo++;
			//SI EL VALOR DE STOCK TIENE DATO ENTONCES COLOCAR EL VALOR SINO DEJAR EL VALOR DE MAESTRO
			$cadena.="\r\n";
			$cadena.= $res[$i]->nro_cont_mst."|".$res[$i]->loc_mst."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->tip_cap."|".$n_simbolo.$n_cant_cap."|".$res[$i]->fec_cong_mst;	
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarprimerarchivo222(){
		$sql="	SELECT CAST(M.nro_cont_mst AS UNSIGNED) nro_cont_mst,CAST(S.nro_cont_stk AS UNSIGNED) nro_cont_stk,M.loc_mst,S.loc_stk,
		C.sku_cap sku_cap,
		RIGHT(CONCAT('0000000000000',C.barra_cap),13) barra_cap,
		IF(c.tip_cap = 5,'05','01') tip_cap,
		C.cant_cap,
		CONCAT(SUBSTRING(M.fec_cong_mst, 7,4),SUBSTRING(M.fec_cong_mst, 4,2),SUBSTRING(M.fec_cong_mst, 1,2)) fec_cong_mst,
		CONCAT(SUBSTRING(S.fec_cong_stk, 7,4),SUBSTRING(S.fec_cong_stk, 4,2),SUBSTRING(S.fec_cong_stk, 1,2)) fec_cong_stk
				FROM maestro M INNER JOIN capturas C
				ON M.sku_barra = C.sku_cap LEFT JOIN stock S
				ON M.sku_barra = S.sku_stk
				group by c.sku_cap";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/IF_CARGA.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;
		$cadena.="\r\n";
		for($i=0;$i<count($res);$i++){
			$cant_cap = $res[$i]->cant_cap;
			if($cant_cap < 0){
				$n_cant_cap = "-" . SUBSTR("00000000000000000" . ($cant_cap * (-1)),-17);
			}else{
				$n_cant_cap = SUBSTR("000000000000000000" . $cant_cap,-18);
			}
			$conteo++;

			//SI EL VALOR DE STOCK TIENE DATO ENTONCES COLOCAR EL VALOR SINO DEJAR EL VALOR DE MAESTRO
			$cadena.="\r\n";
		
			$cadena.= $res[$i]->nro_cont_mst."|".$res[$i]->loc_mst."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->tip_cap."|".$n_cant_cap."|".$res[$i]->fec_cong_mst;
        	
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarsegundoarchivo(){
		/*$sql="	SELECT c.area_cap, c.barra_cap, c.sku_cap, c.des_barra, c.precio_barra, c.jerar, 
				IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as BODEGA, 
				'0.000' as PISO_VENTA 
				FROM capturas c, area_rango r where c.area_cap between r.area_ini_ran and r.area_fin_ran 
				group by c.sku_cap, c.area_cap
				UNION DISTINCT
				SELECT c.area_cap, c.barra_cap, c.sku_cap, c.precio_barra, c.jerar,  
				IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as PISO_VENTA,
				'0.000' as BODEGA
				FROM capturas c,area_rango r where c.area_cap between r.area_ini_ran and r.area_fin_ran 
				group by c.sku_cap, c.area_cap order by sku_cap, area_cap ";*/
				$sql="select CONCAT( REPEAT('0',5 - LENGTH( area_cap) ) , area_cap ) as area_cap, sku_cap, REPLACE(des_barra,'\"','') des_barra, barra_cap, precio_barra, stock, CONCAT( REPEAT('0',9 - LENGTH( jerar) ) , jerar ) AS jerar, BODEGA, PISO_VENTA from corte";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/BOD_SV.txt";
		unlink('$archivo');

		$cadena="\r\n";
		$conteo=1;

		$cadena.="LOTE|SKU|DESCRIPCION|EAN|COSTO|STOCK|JERARQUIA|BODEGA|SALA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->area_cap."|".$res[$i]->sku_cap."|".$res[$i]->des_barra."|".$res[$i]->barra_cap."|".$res[$i]->precio_barra."|".$res[$i]->stock."|".$res[$i]->jerar."|".$res[$i]->BODEGA."|".$res[$i]->PISO_VENTA;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}
/*
	function generarReporteProductividad($estado){
		$sql="	SELECT U.dniUsuario, U.nombreUsuario,
				(SELECT inicioAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia ASC LIMIT 1) inicioAsistencia, 
				(SELECT terminoAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia DESC LIMIT 1) terminoAsistencia,
				SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60) horas_conteo,
				(SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) total_conteo,
				((SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) / SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60)) conteo_x_hora 
				FROM usuario U LEFT JOIN asistencia A
				ON U.dniUsuario = A.dniUsuario
				WHERE U.estadoUsuario = $estado
				GROUP BY U.dniUsuario";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/archivo_reporte_productividad.txt";
		unlink('$archivo');

		$conteo=1;
		$cadena.="\r\n";
		$cadena.="N|DNI|NOMBRE COMPLETO|INICIO CONTEO|FIN CONTEO|HORAS CONTEO|TOTAL CONTADO|CONTEO X HORA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $conteo."|".$res[$i]->dniUsuario."|".$res[$i]->nombreUsuario."|".$res[$i]->inicioAsistencia."|".$res[$i]->terminoAsistencia."|".$res[$i]->horas_conteo."|".$res[$i]->total_conteo."|".$res[$i]->conteo_x_hora;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}
*/

}	
?>