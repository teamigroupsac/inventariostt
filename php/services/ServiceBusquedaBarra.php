<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceBusquedaBarra extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getListaBusquedaBarra($dato){
		$barra_cap = $dato;
		$condicion = "";
		if ($barra_cap > 0){
			$condicion = "WHERE barra_cap = '$dato'";
		}
		$sql = "SELECT A.*, B.nombreUsuario, C.des_barra FROM captura A LEFT JOIN usuario B
                ON A.usuario = B.dniUsuario LEFT JOIN maestro C
                ON A.barra_cap = C.cod_barra $condicion
                ORDER BY A.id_captura ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("fecha","hora","cant_cap","nombreUsuario","des_barra"));

		$sqlBarras = "SELECT COUNT(DISTINCT barra_cap) cont_barra FROM captura $condicion";
		$resBarras = $this->db->get_results($sqlBarras);

		$sqlCantidad = "SELECT SUM(cant_cap) sum_cant FROM captura $condicion";
		$resCantidad = $this->db->get_results($sqlCantidad);

		$registros = new stdClass();
        $registros->registros = $res;
        $registros->barras = $resBarras;
        $registros->cantidad = $resCantidad;

		return $registros;
	}

    function deleteFormularioBusquedaBarra($data){
        $id_captura = $data->id_captura;
        $responsable = $data->responsable;

        $sql="UPDATE captura SET responsable = '$responsable' WHERE id_captura = $id_captura";
        $resEditar=$this->db->query($sql);

        $sql="DELETE FROM captura WHERE id_captura = $id_captura";
        $res=$this->db->query($sql);            

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    /*function modificarMasivoFormularioBusquedaBarra($data){
    	$responsable = $data->responsable;
    	$cant_cap = $data->cant_cap;
    	$capturas = $data->capturas;
    	$usuario = $data->usuario;

    	$listacaptura = implode(",", $capturas);

        $sql="UPDATE captura SET barra_cap = '$barra_cap', cant_cap ='$cant_cap', responsable = '$responsable' WHERE id_captura IN ($listacaptura)";
        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }*/

	function saveFormularioBusquedaBarra($data){
		$procedimiento = $data->procedimiento;
		$id_captura = $data->id_captura;
    	$responsable = $data->responsable;
    	$cant_cap = $data->cant_cap;
		$usuario = $data->usuario;

		if($procedimiento == "MODIFICAR"){
			$sql="UPDATE captura SET 
				cant_cap = $cant_cap,
                responsable = '$responsable'
				WHERE id_captura = $id_captura";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

    function eliminarMasivoFormularioBusquedaBarra($data){
    	$usuarioModificador = $data->usuario;
    	$capturas = $data->capturas;
        $responsablbe = $data->responsable;

    	$listacaptura = implode(",", $capturas);

        $sql="UPDATE captura SET responsable = '$responsable' WHERE id_captura IN ($listacaptura)";
        $resEditar=$this->db->query($sql);


        $sql="DELETE FROM captura WHERE id_captura IN ($listacaptura)";
        $res=$this->db->query($sql);        



        if($res){
            return 1;
        }else{
            return 0;
        }

    }


    function eliminarBarraCapFormularioBusquedaBarra($data){
        $barra_cap = $data->barra;
        $responsable = $data->responsable;

        $sql="UPDATE captura SET responsable = '$responsable' WHERE barra_cap = $barra_cap";
        $resEditar=$this->db->query($sql);


        $sql="DELETE FROM captura WHERE barra_cap = '$barra_cap'";
        $res=$this->db->query($sql);           


        if($res){
            return 1;
        }else{
            return 0;
        }

    }

}	
?>