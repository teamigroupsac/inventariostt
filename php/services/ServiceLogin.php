<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');
require_once("Service.php");

class ServiceLogin extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

	//ACTIVAR ESTA OPCION SI EL SERVIDOR PRINCIPAL CAE O ESTA FUERA DE SERVICIO....
	function validar($usuario,$clave){
			$sql = "SELECT A.idUsuario,A.dniUsuario,A.tipoUsuario,A.nombreUsuario,A.estadoUsuario, B.descripcionEstadoUsuario, C.descripcionTipoUsuario, C.permisos
					FROM usuario A LEFT JOIN estadousuario B
					ON A.estadoUsuario = B.idEstadoUsuario LEFT JOIN tipousuario C
					ON A.tipoUsuario = C.idTipoUsuario 
					WHERE A.dniUsuario = '$usuario' AND A.claveUsuario = '$clave'";
			$res = $this->db->get_results($sql);
			$this->_codificarObjeto($res,array("nombreUsuario","descripcionEstadoUsuario","descripcionTipoUsuario"));
			if($res){
				return $res;
			}else{
				return "ERROR";
			}

	}

}	
?>