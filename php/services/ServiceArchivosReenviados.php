<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceArchivosReenviados extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}



	function listarArchivosReenviados(){

		$archivos = array();

		$directorio = opendir(FILE_FORWARD); //ruta actual
		while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
		{
		    if (is_dir($archivo))//verificamos si es o no un directorio
		    {
		        //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
		    }
		    else
		    {
		    	if(substr($archivo,-4)==".txt"){
			    	$file = new stdClass();
			        $file->nombre = $archivo;
			        $file->peso = filesize(FILE_FORWARD.'/'.$archivo)." KB";
			        $file->fecha = date("Y-m-d", filectime(FILE_FORWARD.'/'.$archivo));

			        $archivos[] = $file;
			    }
		    }

		    
		}

		return $archivos;

	}

	function cargarArchivoReenviadoMasivo($dato){
        $c = 0;
        $reenviado = file(FILE_FORWARD.'/'.$dato);
        foreach ($reenviado as $fila => $valor){
            $c++;

            $valor = str_replace("'","",$valor);
            $valor = str_replace("\n","",$valor);
            $valor = str_replace("\r","",$valor);
            $valor = str_replace("\n\r","",$valor);
            $cadena = explode("\t",$valor);

            $registro = "('".implode("','", $cadena)."','$dato')";

            $sql = "INSERT INTO captura_reenviado(id_captura_reenviado, area_cap, barra_cap, sku_cap, cant_cap, tip_cap, usuario, fecha, hora, descargado, namepalm, archivo) VALUES $registro";

            $res=$this->db->query($sql);


        }

        if($c > 0){
        	copy(FILE_FORWARD.'/'.$dato,"../archivos_sistema/archivos_reprocesados/".$dato);
			unlink(FILE_FORWARD.'/'.$dato);
        }

		$proceso = new stdClass();
		$proceso->registros = $c;
		$proceso->nombre = $dato;

		return $proceso;

	}

	function cargarArchivoReenviado($dato){

        $c = 0;
        $reenviado = file(FILE_FORWARD.'/'.$dato);
        foreach ($reenviado as $fila => $valor){
            $c++;

            $valor = str_replace("'","",$valor);
            $valor = str_replace("\n","",$valor);
            $valor = str_replace("\r","",$valor);
            $valor = str_replace("\n\r","",$valor);
            $cadena = explode("\t",$valor);

            $registro = "('".implode("','", $cadena)."','$dato')";

            $sql = "INSERT INTO captura_reenviado(id_captura_reenviado, area_cap, barra_cap, sku_cap, cant_cap, tip_cap, usuario, fecha, hora, descargado, namepalm, archivo) VALUES $registro";

            $res=$this->db->query($sql);


        }

		$proceso = new stdClass();
		$proceso->registros = $c;
		$proceso->nombre = $dato;

		return $proceso;

	}

	function copiarArchivoReenviado($dato){
		/*$sql="	UPDATE captura SET cant_cap = (cant_cap / 1000)
				WHERE tip_cap = 4";
		$res=$this->db->query($sql);*/

		copy(FILE_FORWARD.'/'.$dato,"../archivos_sistema/archivos_reprocesados/".$dato);
		unlink(FILE_FORWARD.'/'.$dato);

		$total_archivos = count(glob(FILE_FORWARD.'/'.'{*.txt}',GLOB_BRACE));
		return $total_archivos;

	}

	function eliminarArchivoReenviado($dato){
		copy(FILE_FORWARD.'/'.$dato,"../archivos_sistema/archivos_reeliminados/".$dato);
		unlink(FILE_FORWARD.'/'.$dato);

		$total_archivos = count(glob(FILE_FORWARD.'/'.'{*.txt}',GLOB_BRACE));
		return $total_archivos;

	}

	function listarArchivosReenviadosProcesados(){
		$sql = "SELECT archivo,COUNT(archivo) registros, SUM(cant_cap) contado FROM captura_reenviado
				GROUP BY archivo";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function mostrarArchivosReenviadosCargados($archivo){
		$sql = "SELECT A.area_cap,A.descargado,A.usuario,
				COUNT(A.archivo) registros_reenviado,COUNT(B.descargado) registros_captura,
				SUM(A.cant_cap) contado_reenviado, SUM(B.cant_cap) contado_captura,
				(SUM(B.cant_cap) - SUM(A.cant_cap)) diferencia,
				(SELECT GROUP_CONCAT(DISTINCT E.tipo SEPARATOR '-') FROM auditoria E WHERE E.area_cap = A.area_cap) estado_captura, A.estado
				FROM captura_reenviado A LEFT JOIN captura B
				ON CONCAT(A.area_cap,A.barra_cap,A.usuario,A.descargado) = CONCAT(B.area_cap,B.barra_cap,B.usuario,B.descargado)
				WHERE A.archivo = '$archivo'
				GROUP BY A.area_cap,A.descargado,A.usuario";

		$res = $this->db->get_results($sql);
		return $res;
	}

	function reemplazaRegistrosCaptura($dato){
		$area_cap = $dato->area_cap;
		$usuario = $dato->usuario;
		$descargado = $dato->descargado;

		$sqlResponsable = "	UPDATE captura SET responsable = 'IGROUP'
							WHERE area_cap = '$area_cap' AND descargado = '$descargado' AND usuario = '$usuario'";
		$resResponsable = $this->db->query($sqlResponsable);
		
		$sqlEliminar = "	DELETE FROM captura
							WHERE area_cap = '$area_cap' AND descargado = '$descargado' AND usuario = '$usuario'";
		$resEliminar = $this->db->query($sqlEliminar);

		//if($resEliminar){

			$sqlInsertar = "	INSERT INTO captura
								SELECT 0 id_captura,area_cap,barra_cap,sku_cap,cant_cap,tip_cap,usuario,fecha,hora,descargado,namepalm,(NULL) responsable
								FROM captura_reenviado
								WHERE area_cap = '$area_cap' AND descargado = '$descargado' AND usuario = '$usuario'";
			$resInsertar = $this->db->query($sqlInsertar);

			if($resInsertar){

				$sqlActualizar = "	UPDATE captura_reenviado SET estado = 1
									WHERE area_cap = '$area_cap' AND descargado = '$descargado' AND usuario = '$usuario'";
				$resActualizar = $this->db->query($sqlActualizar);

			}

		//}

		$archivo = $this->getDato("archivo","captura_reenviado","area_cap = '$area_cap' AND descargado = '$descargado' AND usuario = '$usuario' LIMIT 1");

		return $archivo;

	}

	function reemplazaTodosRegistrosCaptura($archivo,$descargado){

		$sqlResponsable = "	UPDATE captura SET responsable = 'IGROUP'
							WHERE descargado = '$descargado'";
		$resResponsable = $this->db->query($sqlResponsable);
		
		$sqlEliminar = "	DELETE FROM captura
							WHERE descargado = '$descargado'";//" AND usuario = '$usuario'";
		$resEliminar = $this->db->query($sqlEliminar);

		//if($resEliminar){

			$sqlInsertar = "	INSERT INTO captura
								SELECT 0 id_captura,area_cap,barra_cap,sku_cap,cant_cap,tip_cap,usuario,fecha,hora,descargado,namepalm,(NULL) responsable
								FROM captura_reenviado
								WHERE descargado = '$descargado'";//" AND usuario = '$usuario'";
			$resInsertar = $this->db->query($sqlInsertar);

			if($resInsertar){

				$sqlActualizar = "	UPDATE captura_reenviado SET estado = 1
									WHERE descargado = '$descargado'";//" AND usuario = '$usuario'";
				$resActualizar = $this->db->query($sqlActualizar);

			}

		return $archivo;

	}



}	
?>