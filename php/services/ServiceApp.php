<?php
require_once("ServiceReportes.php");

require_once("ServiceTipoUsuario.php");

require_once("ServiceLogin.php");
require_once("ServiceUsuarios.php");

require_once("ServiceCapturas.php");
require_once("ServiceReporteAuditoria.php");

require_once("ServiceArchivosPendientes.php");
require_once("ServiceArchivosReenviados.php");

require_once("ServiceMaestro.php");
require_once("ServiceStock.php");

require_once("ServiceAreaRango.php");

require_once("ServiceTienda.php");

require_once("ServiceBusquedaBarra.php");

require_once("ServiceCambiaLote.php");

require_once("ServiceGenerarArchivos.php");

require_once("ServiceGenerarCierre.php");

require_once("ServiceCerrarInventario.php");

require_once("ServiceGondola.php");

require_once("ServiceCierre.php");

require_once("ServiceBodega.php");

require_once("ServiceArchivoBarra.php");

require_once("ServiceInforme.php");


require_once("ServiceAdministrativo.php");


require_once("Service.php");
class ServiceApp
{
	private $db;
	private $limiteServidor = 400;
	private $sqlPagina;
	private $idAdmin;
	private $direccion;
	function __construct(	) 
	{
		$GLOBALS['amfphp']['encoding'] = 'amf3';
		$this->db = new ezSQL_mysql(DB_USER,DB_PASS,DB_NAME,DB_HOST);		
		$this->idAdmin = 1;
		$this->direccion = "../../";
		$this->arrayServices = array("ServiceReportes","ServiceTipoUsuario","ServiceLogin","ServiceUsuarios","ServiceCapturas","ServiceReporteAuditoria","ServiceArchivosPendientes","ServiceArchivosReenviados","ServiceMaestro","ServiceStock","ServiceAreaRango","ServiceTienda","ServiceBusquedaBarra","ServiceCambiaLote","ServiceGenerarArchivos","ServiceGenerarCierre","ServiceCerrarInventario","ServiceGondola","ServiceCierre","ServiceBodega","ServiceArchivoBarra","ServiceInforme","ServiceAdministrativo");
	}
	public function ejecutar($funcion,$ar){
		$conexion;
		foreach ($this->arrayServices as $value) {
			$conexion = new $value();
			if( method_exists($conexion, $funcion) ){
				if(PRODUCTION_SERVER)
					$conexion->_iniciar();
				$res = call_user_func_array( array($conexion, $funcion),$ar);
			}
		}
		return $res;
	}	
}	
?>
