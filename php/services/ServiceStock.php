<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceStock extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function saveRegistrosStock($data){
        $c = $g = $f = 0;
        $detalle = "RESUMEN DE CARGA ARCHIVO STOCK \r\n";
        $detalle .= "LINEAS NO CARGADAS \r\n";

        $stock = file("../archivos_sistema/archivos_stock/".$data->archivo);
        $stock_log = fopen("../archivos_sistema/archivos_stock/log_".$data->archivo, "w");

        $sql_truncate = "TRUNCATE TABLE stock";
        $this->db->query($sql_truncate);

        //$registros = array_count_values($stock);
        
        foreach ($stock as $fila => $valor){
            $c++;

            $valor = str_replace("'","",$valor);
            //$valor = str_replace("|","','",$valor);
            //echo $valor;
            //$registro = "('".$valor."');";
            $cadena = explode("|",$valor);

            $registro = "('".implode("','", $cadena)."')"; //stripslashes($cadena)

            $sql = "INSERT INTO stock (nro_cont_stk, fec_cong_stk, sku_stk, loc_stk, tip_inv_stk, des_sku_stk, blanco, uni_vent_stk, cost_prom_stk, mon_stk, sub_clas_stk, des_sub_cla_stk, clas_stk, des_clas_stk, sub_dep_stk, des_sub_dep_stk, dep_stk, des_dep_stk, fec_gen_arc_stk, tip_ean_stk, nro_ean_stk, cant_cer_stk) VALUES $registro";
            $res=$this->db->query($sql);

            if($res){
                $g++;
            }else{
                $f++;
                $detalle .= "LINEA : ".$c." - DETALLE : ".$registro." \r\n";
            }

        }
        $detalle .= " \r\n";
        $detalle .= "FILAS RECORRIDAS : ".$c." \r\n";
        $detalle .= "REGISTROS GUARDADOS : ".$g." \r\n";
        $detalle .= "REGISTROS FALLIDOS : ".$f;

        fwrite($stock_log, $detalle);
        fclose($stock_log);

        return $c;
    }

    function listarArchivosStockPendientes(){

        $archivos = array();

        $directorio = opendir("../archivos_sistema/archivos_stock"); //ruta actual
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {
            if (is_dir($archivo))//verificamos si es o no un directorio
            {
                //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
            }
            else
            {

                $esArchivo = strpos($archivo, "log");

                if ($esArchivo === false) {

                    $bytes = filesize("../archivos_sistema/archivos_stock/".$archivo);
                    $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
                    for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
                    $peso = ( round( $bytes, 2 ) . " " . $label[$i] );


                    $file = new stdClass();
                    $file->nombre = $archivo;
                    $file->log = "log_".$archivo;
                    $file->peso = $peso;
                    $file->fecha = date("Y-m-d", filectime("../archivos_sistema/archivos_stock/".$archivo));

                    $archivos[] = $file;

                }
            }

            
        }

        return $archivos;

    }

    function eliminarArchivoStockPendiente($dato){
        unlink("../archivos_sistema/archivos_stock/".$dato);
        unlink("../archivos_sistema/archivos_stock/log_".$dato);
        return 1;

    }




}	
?>