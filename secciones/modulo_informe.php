                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="informe">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            INFORME DE INVENTARIO
                        </div>
                        <div class="panel-body">
                        <br>
                        
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#almacenBodega" data-toggle="tab" aria-expanded="false">ALMACEN - BODEGA</a>
                            </li>
                            <li><a href="#pisoVenta" data-toggle="tab" aria-expanded="false">PISO DE VENTA</a>
                            </li>
                            <li><a href="#configurarPreguntas" data-toggle="tab" aria-expanded="false">CONFIGURAR PREGUNTAS</a>
                            </li>
                            <!--
                            <li><a href="#configurarRespuestas" data-toggle="tab" aria-expanded="false">CONFIGURAR RESPUESTAS</a>
                            </li>
                            <li><a href="#configurarGruposCuestionarios" data-toggle="tab" aria-expanded="false">CONFIGURACION GRUPOS DE RESPUESTA Y CUESTIONARIOS</a>
                            </li>
                            -->
                        </ul>

                        <!-- Tab panes --> 
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="almacenBodega">
                                <br>                               
                                <div class="contenedor">
                                    <div class="row">
                                        <div class="col-md-12 texto negrita centro titulo_informe">INFORME DE INVENTARIO</div>
                                    </div> 
                                    <div class="row">
                                        <div class="col-md-9"></div>
                                        <div class="col-md-1 texto centro negrita">FECHA</div>
                                        <div class="col-md-2">
                                            <div class='input-group date' id='datetimepickerFechaAB'>
                                                <input type='text' class="form-control input-sm fechaAlmacenBodega"></input>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9"></div>
                                        <div class="col-md-1 texto centro negrita">N° TIENDA</div>
                                        <div class="col-md-2">
                                            <input type='number' class="form-control input-sm numeroTiendaAlmacenBodega">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2 texto negrita">RESPONSABLE IGROUP</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm responsableIgroupAlmacenBodega">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">RESPONSABLE CLIENTE</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm responsableClienteAlmacenBodega">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">TIENDA / LOCAL</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm nombreTiendaAlmacenBodega">
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row">
                                        <div class="col-md-12 texto negrita titulo_informe">INVENTARIO - ALMACEN / BODEGA</div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">ENCARGADO CLIENTE</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm encargadoClienteAlmacenBodega">
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1 texto negrita">INICIO CONTEO</div>
                                        <div class="col-md-2">
                                            <input type='text' class="form-control input-sm inicioConteoAlmacenBodega">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">SUPERVISOR IGROUP</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm supervisorIgroupAlmacenBodega">
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1 texto negrita">FIN DE CONTEO</div>
                                        <div class="col-md-2">
                                            <input type='text' class="form-control input-sm finConteoAlmacenBodega">
                                        </div>
                                    </div>
                                    <br>


                                    <div class="cuestionario">ssss</div>


                                    <br>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">OBSERVACIONES</div>
                                        <div class="col-md-10">
                                            <textarea rows="3" placeholder="OBSERVACIONES" class="form-control input-sm observacionesAlmacenBodega"></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
<!--
                                    <div class="row">
                                        <div class="col-md-6 centro">
                                            <button id="botonFirmaSupervidorIgroupAlmacenBodega" type="button" class="btn btn-primary btn-sm">FIRMA ENCARGADO CLIENTE</button>
                                        </div>
                                        <div class="col-md-6 centro">
                                            <button id="botonFirmaSupervidorIgroupAlmacenBodega" type="button" class="btn btn-primary btn-sm">FIRMA SUPERVISOR IGROUP</button>
                                        </div>
                                    </div>
-->
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-success btn-sm btn-block botonGuardarAlmacenBodega">GUARDAR INFORME</button>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-warning btn-sm btn-block botonLimpiarAlmacenBodega">LIMPIAR INFORME</button>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-danger btn-sm btn-block botonCancelarAlmacenBodega">CANCELAR CAMBIOS</button>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-info btn-sm btn-block botonImprimirAlmacenBodega">IMPRIMIR INFORME</button>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pisoVenta">
                                <br>                               
                                <div class="contenedor">
                                    <div class="row">
                                        <div class="col-md-12 texto negrita centro titulo_informe">INFORME DE INVENTARIO</div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 texto negrita titulo_informe">INVENTARIO - PISO DE VENTA</div>
                                    </div>


                                    <br>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">ENCARGADO CLIENTE</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm encargadoClientePisoVenta">
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1 texto negrita">INICIO CONTEO</div>
                                        <div class="col-md-2">
                                            <input type='text' class="form-control input-sm inicioConteoPisoVenta">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">SUPERVISOR IGROUP</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm supervisorIgroupPisoVenta">
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1 texto negrita">FIN DE CONTEO</div>
                                        <div class="col-md-2">
                                            <input type='text' class="form-control input-sm finConteoPisoVenta">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">MESA DE CONTROL</div>
                                        <div class="col-md-6">
                                            <input type='text' class="form-control input-sm mesaControlPisoVenta">
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1 texto negrita">CIERRE INVENTARIO</div>
                                        <div class="col-md-2">
                                            <input type='text' class="form-control input-sm cierreInventarioPisoVenta">
                                        </div>
                                    </div>
                                    <br>


                                    <div class="cuestionario"></div>


                                    <br>
                                    <div class="row">
                                        <div class="col-md-2 texto negrita">OBSERVACIONES</div>
                                        <div class="col-md-10">
                                            <textarea rows="3" placeholder="OBSERVACIONES" class="form-control input-sm observacionesPisoVenta"></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
<!--
                                    <div class="row">
                                        <div class="col-md-6 centro">
                                            <button id="botonFirmaSupervidorIgroupPisoVenta" type="button" class="btn btn-primary btn-sm">FIRMA ENCARGADO CLIENTE</button>
                                        </div>
                                        <div class="col-md-6 centro">
                                            <button id="botonFirmaSupervidorIgroupPisoVenta" type="button" class="btn btn-primary btn-sm">FIRMA SUPERVISOR IGROUP</button>
                                        </div>
                                    </div>
-->
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-success btn-sm btn-block botonGuardarPisoVenta">GUARDAR INFORME</button>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-warning btn-sm btn-block botonLimpiarPisoVenta">LIMPIAR INFORME</button>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-danger btn-sm btn-block botonCancelarPisoVenta">CANCELAR CAMBIOS</button>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-info btn-sm btn-block botonImprimirPisoVenta">IMPRIMIR INFORME</button>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="configurarPreguntas">
                                <br>                               
                                <div class="contenedor">

                                    <div class="row">
                                        <div class="col-md-12 texto negrita centro titulo_informe">MANTENIMIENTO DE PREGUNTAS</div>
                                    </div>


                                    <br>
                                    <div class="row contenedor opcionesPregunta">
                                        <div class="col-md-1 texto negrita">PREGUNTA</div>
                                        <div class="col-md-6">
                                            <input type='hidden' class="form-control input-sm codigoPregunta">
                                            <input type='text' class="form-control input-sm nombrePregunta">
                                        </div>
                                        <div class="col-md-1 texto negrita">ORDEN</div>
                                        <div class="col-md-1">
                                            <input type='number' class="form-control input-sm ordenPregunta">
                                        </div>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-1">
                                            <button type="button" class="btn btn-success btn-sm btn-block guardarPregunta">GUARDAR</button>
                                        </div>
                                    </div>
                                    <div class="row contenedor opcionesPregunta">
                                        <div class="col-md-1 texto negrita">CUESTIONARIO</div>
                                        <div class="col-md-3">
                                            <select class="form-control input-sm cuestionarioPregunta">
                                                <option value='0'>---SELECCIONAR---</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 texto negrita">GRUPO DE RESPUESTA</div>
                                        <div class="col-md-3">
                                            <select class="form-control input-sm grupoPregunta">
                                                <option value='0'>---SELECCIONAR---</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-1">
                                            <button type="button" class="btn btn-danger btn-sm btn-block cancelarPregunta">CANCELAR</button>
                                        </div>
                                    </div>

                                    <div class="row contenedor opcionesPreguntaMasivo" style="display: none;">
                                        <div class="col-md-2"></div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table width="100%" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>PREGUNTA</th>
                                                        <th>ORDEN</th>
                                                        <th>CUESTIONARIO</th>
                                                        <th>GRUPO</th>
                                                        <th>SELECCIONAR</th>
                                                        <th>OPCIONES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr></tr>                                              
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="tab-pane fade" id="configurarRespuestas">
                                <br>                               
                                <div class="contenedor">
                                    <div class="row">
                                        <div class="col-md-8">

                                            <div class="panel panel-primary respuestas">
                                                <div class="panel-heading">
                                                    MANTENIMIENTO DE RESPUESTAS
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4">

                                            <div class="panel panel-default grupos">
                                                <div class="panel-heading">
                                                    MANTENIMIENTO DE GRUPOS
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="configurarGruposCuestionarios">
                                e
                            </div>
                        </div>




                        </div>
                    </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                </div>

