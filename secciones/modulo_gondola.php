                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="gondola">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO GONDOLA
                        </div>
                        <div class="panel-body">

                           

                            <br>
                            <div class="row contenedor">
                                <div class="col-md-12">
                                    <input type='file' id="archivoGondola" style='visibility:hidden; height:0'>
                                    <div class="form-group">
                                        <div class="input-group" name="Fichero">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary boton-carga-gondola btn-sm" type="button">BUSCAR ARCHIVO GONDOLA</button>
                                            </span>
                                            <input type="text" class="form-control campo-carga-gondola input-sm" placeholder='NOMBRE ARCHIVO GONDOLA...' />
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger boton-limpia-gondola btn-sm" type="button">RESETEAR</button>
                                                <button class="btn btn-success submit-carga-gondola btn-sm" type="button" disabled>CARGAR ARCHIVO Y PROCESAR</button>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row contenedor progresoCargandoGondola" style="display:none">
                                <div class="col-md-12">
                                    <div class="progress">
                                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">CARGANDO REGISTROS A LA BASE DE DATOS</div>
                                    </div>                                
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            INFORMACION DEL ARCHIVO CARGADO
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3 texto negrita">TAMAÑO</div>
                                                <div class="col-md-3 texto peso">0</div>
                                                <div class="col-md-3 texto negrita">REGISTROS</div>
                                                <div class="col-md-3 texto filas">0</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita"></div>
                                                <div class="col-md-3 texto"></div>
                                                <div class="col-md-3 texto negrita">FECHA</div>
                                                <div class="col-md-3 texto fecha"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-8">

                                    <div class="panel panel-primary archivos_generados">
                                        <div class="panel-heading">
                                            GENERAR ARCHIVOS
                                        </div>
                                        <div class="panel-body">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="col-md-4">
                                                    <button type="button" class="btn btn-default btn-sm btn-block areadetalle">AREA DETALLE</button>
                                                </div>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-4">
                                                    <button type="button" class="btn btn-default btn-sm btn-block contadosurtido">CONTADO SURTIDO</button>
                                                </div>
                                                <div class="col-md-1"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="col-md-2 texto negrita">REGISTROS</div>
                                                <div class="col-md-2 texto filas_1">0</div>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-2 texto negrita">REGISTROS</div>
                                                <div class="col-md-2 texto filas_2">0</div>
                                                <div class="col-md-1"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="col-md-2 texto negrita">TAMAÑO</div>
                                                <div class="col-md-2 texto peso_1">0</div>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-2 texto negrita">TAMAÑO</div>
                                                <div class="col-md-2 texto peso_2">0</div>
                                                <div class="col-md-1"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="col-md-2 texto negrita">FECHA</div>
                                                <div class="col-md-2 texto fecha_1"></div>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-2 texto negrita">FECHA</div>
                                                <div class="col-md-2 texto fecha_2"></div>
                                                <div class="col-md-1"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="col-md-4">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargarareadetalle">DESCARGAR</button>
                                                </div>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-4">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargarcontadosurtido">DESCARGAR</button>
                                                </div>
                                                <div class="col-md-1"></div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>


                            <br>                                                              



                    </div>
                </div>
            </div>

