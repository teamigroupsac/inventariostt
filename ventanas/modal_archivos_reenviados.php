        <div class="modal fade" id="modal_registros_archivo_seleccionado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">CAPTURA DE ARCHIVO SELECCIONADO</h4>
              </div>
              <div class="modal-body">
            
                <div class="row">
                  <div class="col-md-12">
                    <button id="botonRegistrarTodos" type="button" class="btn btn-info btn-sm">REGISTRAR TODAS LAS AREAS</button>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">

                            <table id="tablaRegistrosArchivoSeleccionado" width="100%" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>LOTE</th>
                                        <th>USUARIO</th>
                                        <th>REG_REENVIADO</th>
                                        <th>CANT_REENVIADO</th>
                                        <th>REG_CAPTURA</th>
                                        <th>CANT_CAPTURA</th>
                                        <th>DIFERENCIA</th>
                                        <th>ESTADO_CAPTURA</th>
                                        <th width="15%">OPCIONES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                  </div>
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
              </div>
            </div>
          </div>
        </div>