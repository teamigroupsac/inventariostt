var service = new Service("webService/index.php");


$(function() 
{

iniciarControlTienda();

});

var arrayTiendas = [];

function iniciarControlTienda(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $('#fechaFormularioTienda').val(fecha);

    



    $("#menu_tienda").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_tienda").on('click', function(){
      cargaConsultasIniciales();  
    })   


    function cargaConsultasIniciales(){
        service.procesar("getListaTienda",cargaListaTienda);
        service.procesar("getListaClientes",cargaListaClientes);
        //service.procesar("getListaEstados",cargaListaEstados);
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }

    function cargaListaClientes(evt){
        resultado = evt;
        $("#idCliente").html("<option value=''>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#idCliente" ).append( "<option value='"+ resultado[i].dniUsuario +"'>"+ resultado[i].nombreUsuario +"</option>" );
        }
    } 



    function cargaListaTienda(evt){
        listadeTienda = evt;
        $("#tablaResultadoFormularioTienda tbody").html("");

        if ( listadeTienda == undefined ) return;

        for(var i=0; i<listadeTienda.length ; i++){

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            datoRegistro = listadeTienda[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeTienda[i].numeroTienda +'</td><td>'+ listadeTienda[i].nombreTienda +'</td><td>'+ listadeTienda[i].nombreUsuario +'</td><td>'+ listadeTienda[i].fechaTienda +'</td>');

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar'>");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",datoRegistro);
            chkSeleccionar.on("change",seleccionarFormularioTienda);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);


            var contenedorBotones = $("<td>");

            

            var btnSeleccionar = $("<button class='btn btn-primary btn-sm botonesTienda' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span> EDITAR');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarFormularioTienda);

            var btnEliminar = $("<button class='btn btn-danger btn-sm botonesTienda' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarFormularioTienda);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaResultadoFormularioTienda tbody").append(fila);

        }

    }

    function seleccionarFormularioTienda(){
        var data = $(this).data("data");
        var idTienda = data.idTienda;
        var valor = $.inArray(idTienda, arrayTiendas);

        if(valor >= 0){
        	removeItemFromArr( arrayTiendas, idTienda );
        }else{
        	arrayTiendas.push(idTienda);
        }
        
        var contar = arrayTiendas.length;

        if(contar > 1){
        	$(".opcionestienda").css("display","none");
            $(".botonestienda").prop( "disabled", true );
        	$(".opcionestiendamasivo").css("display","");
        }else{
        	$(".opcionestienda").css("display","");
            $(".botonestienda").prop( "disabled", false );
        	$(".opcionestiendamasivo").css("display","none");
        }
    }

    function editarFormularioTienda(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DE LA TIENDA : " + data.nombreTienda +" ?", function (e) {
            if (e) {
                $("#codigoFormularioTienda").val(data.idTienda);
                $("#numeroFormularioTienda").val(data.numeroTienda);
                $("#nombreFormularioTienda").val(data.nombreTienda);
                $("#idCliente").val(data.cliente);
                $("#fechaFormularioTienda").val(data.fechaTienda);

                $("#botonGuardarTienda").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarFormularioTienda(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR TIENDA : " + data.nombreTienda +" ?", function (e) {
            if (e) {
                service.procesar("deleteFormularioTienda",data.idTienda,resultadoDeleteFormularioTienda);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteFormularioTienda(evt){
        service.procesar("getListaTienda",cargaListaTienda);
    }

    $("#botonGuardarTienda").on('click', function(){

        var procedimiento = $("#botonGuardarTienda").html();
        var idTienda= $("#codigoFormularioTienda").val();

        var numeroTienda = $("#numeroFormularioTienda").val();
        var nombreTienda  = $("#nombreFormularioTienda").val();
        var nombreUsuario = $("#idCliente").val();
        var fechaTienda = $("#fechaFormularioTienda").val();

        if(numeroTienda != "" && nombreTienda != "" && fechaTienda != "0"){

            alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                if (e) {

                    var objetoFormulario = new Object()
                        objetoFormulario.procedimiento = procedimiento;
                        objetoFormulario.idTienda = idTienda;
                        objetoFormulario.numeroTienda = numeroTienda;
                        objetoFormulario.nombreTienda = nombreTienda;
                        objetoFormulario.nombreUsuario = nombreUsuario;
                        objetoFormulario.fechaTienda = fechaTienda;

                    service.procesar("saveFormularioTienda",objetoFormulario,cargaFormularioTienda);

                    alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });

        }else{
            alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
        }



    })

    $("#botonCancelarTienda").on('click',function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCEDO ?", function (e) {
            if (e) {
		        alertify.success("PROCESO CANCELADO");
		        formularioTiendaEstandar();
            }
        });
    })


    $("#botonEliminarMasivoFormularioTienda").on('click',function(){
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS MASIVOS ?", function (e) {
            if (e) {

                var objetoFormulario = new Object()
                    objetoFormulario.tiendas = arrayTiendas;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarMasivoFormularioTienda",objetoFormulario,cargaEliminarMasivoFormularioTienda);

                alertify.success("HA ACEPTADO ELIMINAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    })

    function cargaEliminarMasivoFormularioTienda(evt){
    	if(evt == 1 ){
            alertify.success("REGISTROS ELIMINADOS SATISFACTORIAMENTE");
            $(".opcionestienda").css("display","");
        	$(".opcionestiendamasivo").css("display","none");
        	arrayTiendas.length=0;
            service.procesar("getListaTienda",cargaListaTienda);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaModificarMasivoFormularioTienda(evt){
    	if(evt == 1 ){
            alertify.success("REGISTROS GUARDADOS SATISFACTORIAMENTE");
            $(".opcionestienda").css("display","");
        	$(".opcionestiendamasivo").css("display","none");
        	arrayTiendas.length=0;
            service.procesar("getListaTienda",cargaListaTienda);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaFormularioTienda(evt){
        if(evt == 1 ){
            alertify.success("REGISTRO GUARDADO SATISFACTORIAMENTE");
            service.procesar("getListaTienda",cargaListaTienda);
            formularioTiendaEstandar();
        }else if(evt == 2 ){
            alertify.success("REGISTRO MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getListaTienda",cargaListaTienda);
            formularioTiendaEstandar();
        }else{
            alertify.error("REGISTRO NO GUARDADO"); 
        }
    }

    function formularioTiendaEstandar(){
        $("#numeroFormularioTienda").val("");
        $("#nombreFormularioTienda").val("");
        $("#idCliente").val("");
        $("#fechaFormularioTienda").val(fecha);
        $("#botonGuardarTienda").html("GUARDAR");
    }


    $("#botonListarImprimirTienda").on('click',function(){
        window.open("reportes/reporteTienda.php");
    })









    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


