var service = new Service("webService/index.php");


$(function() 
{

iniciarControlGondola();

});

var arrayCapturas = [];

function iniciarControlGondola(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");

    var archivo = $("#archivoGondola");
    //$('#fechaFormularioUsuario').val(fecha);

    




    $("#menu_gondola").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_gondola").on('click', function(){
      cargaConsultasIniciales();  
    })


    function cargaConsultasIniciales(){
        service.procesar("comprobarInformacionArchivo",resultadoGondola);

        var file = "area_detalle";
        service.procesar("comprobarInformacionArchivosGenerados",file,resultadoGenerarAreaDetalle);
        var file = "contado_surtido";
        service.procesar("comprobarInformacionArchivosGenerados",file,resultadoGenerarContadoSurtido);

        //$(".opcionesCaptura").css("display","none");
        //$(".opcionesMasivoCaptura").css("display","none");
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }

    
    $("#fileCargaGondola").on("click",function(){
        $("#inputCargaGondola").trigger("click")
        alertify.success("PESO MAXIMO 100MB");
    })



    $(".boton-carga-gondola").on('click',function(){
        archivo.trigger("click");

        archivo.change(function(){
            $(".campo-carga-gondola").val( (archivo.val()).split('\\').pop() );
            $(".submit-carga-gondola").prop("disabled",false);
        });
        

    });

    $(".boton-limpia-gondola").on('click',function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR LA INFORMACION ?", function (e) {
            if (e) {
                limpiarCargaGondola();
            }
        });
    })

    $(".submit-carga-gondola").on('click',function(){
        alertify.confirm("¿ SEGURO DE GUARDAR INFORMACION DEL ARCHIVO ?", function (e) {
            if (e) {
                //console.log(archivo[0].files[0].name);
                var valorArchivo = archivo[0].files[0];
                subirArchivo(valorArchivo,0,0);            
            }
        });

    })

    function limpiarCargaGondola(){
        archivo.val(null);
        $(".campo-carga-gondola").val("");
        $(".submit-carga-gondola").prop("disabled",true);
    }

    $("#gondola .archivos_generados .areadetalle").on('click',function(){

    })

    $("#gondola .archivos_generados .contadosurtido").on('click',function(){
        
    })





    $("#gondola .archivos_generados .areadetalle").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO AREA DETALLE ?", function (e) {
            if (e) {
                openModalGenerarArchivo("AREA DETALLE");
                service.procesar("generarareadetalle",function(evt){
                    resultadoGenerarAreaDetalle(evt);
                    closeModalGenerarArchivo();
                })
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarAreaDetalle(evt){
        var resultado = evt;
        $("#gondola .archivos_generados .peso_1").html(resultado[0].peso);
        $("#gondola .archivos_generados .filas_1").html(resultado[0].filas);
        $("#gondola .archivos_generados .fecha_1").html(resultado[0].fecha);
    }

    $("#gondola .archivos_generados .descargarareadetalle").on('click', function(){
        var file = 'area_detalle';
        open("webService/descargarArchivoGondola.php?file="+file);
    })


    $("#gondola .archivos_generados .contadosurtido").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO CONTADO SURTIDO ?", function (e) {
            if (e) {
                openModalGenerarArchivo("CONTADO SURTIDO");
                service.procesar("generarcontadosurtido",function(evt){
                    resultadoGenerarContadoSurtido(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarContadoSurtido(evt){
        var resultado = evt;
        $("#gondola .archivos_generados .peso_2").html(resultado[0].peso);
        $("#gondola .archivos_generados .filas_2").html(resultado[0].filas);
        $("#gondola .archivos_generados .fecha_2").html(resultado[0].fecha);
    }

    $("#gondola .archivos_generados .descargarcontadosurtido").on('click', function(){
        var file = 'contado_surtido';
        open("webService/descargarArchivoGondola.php?file="+file);
    })


    function openModalGenerarArchivo(archivo){
        $("#modal_gondola_progress").modal('show');
        $("#modal_gondola_progress .descripcionarchivogenerado").html("GENERANDO ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(){
        $("#modal_gondola_progress").modal('hide');
        alertify.success("ARCHIVO GENERADO SATISFACTORIAMENTE");
    }
























    function subirArchivo(archivo,datos,funcion){
        var data = new FormData();
        data.append("file",archivo);
        $.ajax({
            type: "POST",
            contentType: false,
            url: "webService/guardarArchivoGondola.php",
            data: data,
            cache: false,
            processData: false,
            dataType:"json",
            success: function(evt){
                datos.link = evt.nombre;
                alertify.success("ARCHIVO CARGADO AL SERVIDOR");
                $(".progresoCargandoGondola").css("display","");
                //limpiarCargaGondola();
                var objDatos = new Object()
                    objDatos.archivo = evt.nombre;
                service.procesar("saveRegistrosGondola",objDatos,mensajeCargaGondola);

            },
            error: function(evt){
                alertify.error("ARCHIVO NO CARGADO");
            }
        });
    }

    function mensajeCargaGondola(evt){
        $(".progresoCargandoGondola").css("display","none");
        limpiarCargaGondola();
        //service.procesar("listarArchivosGondola",cargaListarArchivosGondola);
        alertify.success("INFORMACION PROCESADA SATISFACTORIAMENTE");
    }

    function resultadoGondola(evt){
        var resultado = evt;
        $("#gondola .peso").html(resultado[0].peso);
        $("#gondola .filas").html(resultado[0].filas);
        $("#gondola .fecha").html(resultado[0].fecha);
    }









    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


