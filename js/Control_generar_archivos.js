var service = new Service("webService/index.php");


$(function() 
{

iniciarControlGenerarArchivos();

});

var arrayTiendas = [];

function iniciarControlGenerarArchivos(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $('#fechaFormularioTienda').val(fecha);

    



    $("#menu_generar_archivos").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_generar_archivos").on('click', function(){
      cargaConsultasIniciales();  
    })
    
    function cargaConsultasIniciales(){
        var file = "usuario";
        service.procesar("comprobarInformacionArchivoGen",file,resultadoGenerarArchivoUsuario);
        var file = "maestro";
        service.procesar("comprobarInformacionArchivoGen",file,resultadoGenerarArchivoMaestro);
        var file = "maestro_detalle";
        service.procesar("comprobarInformacionArchivoGen",file,resultadoGenerarArchivoMaestroDetalle);
        var file = "tottus_1";
        service.procesar("comprobarInformacionArchivoGen",file,resultadoGenerarArchivoTottus1);
        var file = "tottus_2";
        service.procesar("comprobarInformacionArchivoGen",file,resultadoGenerarArchivoTottus2);
        //var file = "reporte_productividad";
        //service.procesar("comprobarInformacionArchivo",file,resultadoGenerarArchivoReporteProductividad);
    }


    $("#generar_archivos .usuarios .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO USUARIO ?", function (e) {
            if (e) {
                openModalGenerarArchivo("USUARIO");
                service.procesar("generarArchivoUsuario",1,function(evt){
                    resultadoGenerarArchivoUsuario(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoUsuario(evt){
        var resultado = evt;
        $("#generar_archivos .usuarios .peso").html(resultado[0].peso);
        $("#generar_archivos .usuarios .filas").html(resultado[0].filas);
        $("#generar_archivos .usuarios .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .usuarios .descargararchivo").on('click', function(){
        var file = 'usuario';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })


    $("#generar_archivos .maestro .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO MAESTRO ?", function (e) {
            if (e) {
                openModalGenerarArchivo("MAESTRO");
                service.procesar("generarArchivoMaestro",function(evt){
                    resultadoGenerarArchivoMaestro(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoMaestro(evt){
        var resultado = evt;
        $("#generar_archivos .maestro .peso").html(resultado[0].peso);
        $("#generar_archivos .maestro .filas").html(resultado[0].filas);
        $("#generar_archivos .maestro .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .maestro .descargararchivo").on('click', function(){
        var file = 'maestro';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })


    $("#generar_archivos .maestro-detalle .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO MAESTRO DETALLE ?", function (e) {
            if (e) {
                openModalGenerarArchivo("MAESTRO DETALLE");
                service.procesar("generarArchivoMaestroDetalle",function(evt){
                    resultadoGenerarArchivoMaestroDetalle(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoMaestroDetalle(evt){
        var resultado = evt;
        $("#generar_archivos .maestro-detalle .peso").html(resultado[0].peso);
        $("#generar_archivos .maestro-detalle .filas").html(resultado[0].filas);
        $("#generar_archivos .maestro-detalle .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .maestro-detalle .descargararchivo").on('click', function(){
        var file = 'maestro_detalle';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })



    function openModalGenerarArchivo(archivo){
        $("#modal_generar_archivos_progress").modal('show');
        $("#modal_generar_archivos_progress .descripcionarchivogenerado").html("GENERANDO ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(){
        $("#modal_generar_archivos_progress").modal('hide');
        alertify.success("ARCHIVO ACTUALIZADO SATISFACTORIAMENTE");
    }




    $("#generar_archivos .archivo-tottus-1 .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO TOTTUS FORMATO 1 ?", function (e) {
            if (e) {
                openModalGenerarArchivo("PRIMER ARCHIVO");
                service.procesar("generarprimerarchivo",function(evt){
                    resultadoGenerarArchivoTottus1(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoTottus1(evt){
        var resultado = evt;
        $("#generar_archivos .archivo-tottus-1 .peso").html(resultado[0].peso);
        $("#generar_archivos .archivo-tottus-1 .filas").html(resultado[0].filas);
        $("#generar_archivos .archivo-tottus-1 .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .archivo-tottus-1 .descargararchivo").on('click', function(){
        var file = 'IF_CARGA';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })


    $("#generar_archivos .archivo-tottus-2 .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO TOTTUS FORMATO 2 ?", function (e) {
            if (e) {
                openModalGenerarArchivo("SEGUNDO ARCHIVO");
                service.procesar("generarsegundoarchivo",function(evt){
                    resultadoGenerarArchivoTottus2(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoTottus2(evt){
        var resultado = evt;
        $("#generar_archivos .archivo-tottus-2 .peso").html(resultado[0].peso);
        $("#generar_archivos .archivo-tottus-2 .filas").html(resultado[0].filas);
        $("#generar_archivos .archivo-tottus-2 .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .archivo-tottus-2 .descargararchivo").on('click', function(){
        var file = 'BOD_SV';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })

/*
    $("#generar_archivos .archivo-reporte-productividad .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO DE REPORTE DE PRODUCTIVIDAD ?", function (e) {
            if (e) {
                var estado = 1;
                service.procesar("generarReporteProductividad",estado,resultadoGenerarArchivoReporteProductividad);
                openModalGenerarArchivo("REPORTE DE PRODUCTIVIDAD");
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoReporteProductividad(evt){
        var resultado = evt;
        $("#generar_archivos .archivo-reporte-productividad .peso").html(resultado[0].peso);
        $("#generar_archivos .archivo-reporte-productividad .filas").html(resultado[0].filas);
        $("#generar_archivos .archivo-reporte-productividad .fecha").html(resultado[0].fecha);
        closeModalGenerarArchivo();
    }

    $("#generar_archivos .archivo-reporte-productividad .descargararchivo").on('click', function(){
        var file = 'reporte_productividad';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })

    $("#generar_archivos .archivo-reporte-productividad .imprimirarchivo").on('click', function(){
        var estado = 1;
        window.open("reportes/reporteProductividad.php?estado="+estado);
    })
*/



    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

}


