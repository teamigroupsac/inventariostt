var service = new Service("webService/index.php");


$(function() 
{

iniciarControlCierre();

});


function iniciarControlCierre(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;


    $("#menu_cierre").on('click', function(){
      	$("#modal_cierre_inventario").modal("show");
    	service.procesar("comprobarInformacionCierre",resultadoInformacionCierre);
    })
    $("#boton_menu_cierre").on('click', function(){
        $("#modal_cierre_inventario").modal("show");
        service.procesar("comprobarInformacionCierre",resultadoInformacionCierre);
    })


    function resultadoInformacionCierre(evt){
        var resultado = evt;
        $("#modal_cierre_inventario .fecha").html(resultado);
    }

    $("#modal_cierre_inventario .generarcierre").on('click', function(){

        service.procesar("consultarRegistrosStock",function(evt){
            var resultado = parseFloat(evt);
            if(resultado <= 0){
                alertify.error("TABLA STOCK SIN REGISTROS");
            }
        });

        service.procesar("existenArchivosPendientes",function(evt){

            if(evt > 0){

                alertify.error("EXISTEN DESCARGAR PENDIENTES, PROCESARLAS ANTES DE GENERAR CIERRE");

            }else{

                alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO DE CIERRE ?", function (e) {
                    if (e) {
                        //$("#modal_generar_cierre_progress").modal('show');
                        openModalGenerarArchivo("CIERRE");
                        service.procesar("generarCierre",function(evt){
                            if(evt == 1){
                                alertify.success("DIFERENCIA GENERADA SATISFACTORIAMENTE");    
                                service.procesar("comprobarInformacionCierre",resultadoInformacionCierre);
                            }else{
                                alertify.error("DIFERENCIA NO GENERADA, REVISAR CONFLICTOS");
                            }
                            closeModalGenerarArchivo();
                        });
                    } else {
                        alertify.error("HA CANCELADO EL PROCESO");
                    }
                });

            }

        })            

    })


    $("#modal_cierre_inventario .sincronizarInventario").on('click', function(){

        alertify.confirm("¿ SEGURO DE ENVIAR INFORMACION DE INVENTARIO AL SISTEMA WEB ?", function (e) {
            if (e) {
                //$("#modal_generar_cierre_progress").modal('show');
                openModalGenerarArchivo("SINCRONIZANDO");
                service.procesar("sincronizarInventario",function(evt){
                    if(evt == 1){
                        alertify.success("INFORMACION ENVIADA AL SISTEMA WEB");    
                        //service.procesar("comprobarInformacionCierre",resultadoInformacionCierre);
                    }else{
                        alertify.error("INFORMACION NO ENVIADA AL SISTEMA WEB");
                    }
                    console.log("cerrado");
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });

    })

    function openModalGenerarArchivo(archivo){
        $("#modal_cierre_progress").modal('show');
        $("#modal_cierre_progress .descripcionarchivogenerado").html("GENERANDO ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(){
        $("#modal_cierre_progress").modal('hide');
        //alertify.success("PROCESO GENERADO SATISFACTORIAMENTE");
    }



}


