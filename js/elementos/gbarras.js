var service = new Service("webService/index.php");


$(function() {

iniciarAppResultados();

})

function iniciarAppResultados(){

var lista_categorias = $("#categorias");
var lista_colaboradores = $("#colaboradores");
var lista_tipo_estudio = $("#tipodeestudio");

service.procesar("getReportes",crearGraficoReportes);

lista_categorias.change(function(){

service.procesar("getReportes",lista_categorias.val(),lista_colaboradores.val(),crearGraficoReportes);

});

lista_colaboradores.change(function(){

service.procesar("getReportes",lista_categorias.val(),lista_colaboradores.val(),crearGraficoReportes);

});


        function crearGraficoReportes(evt) {
            datos = evt;
            categorias = datos.categorias;
            resultados = datos.usuarios;

            $("#greportes").kendoChart({
                dataSource: {
                    data: resultados
                },
                title: {
                    text: "AVANCE DE ENCUESTAS"
                },
                legend: {
                    position: "right"
                },
                chartArea: {
                    background: ""
                },
                seriesDefaults: {
                    type: "column",
                    style: "smooth"
                },

                series: categorias,


                valueAxis: {
                    max: 10,
                    majorUnit: 1,
                    line: {
                        visible: true
                    },
                    plotBands: [{
                        from: 0,
                        to: 1.95,
                        color: "#c00",
                        opacity: 0.3
                    }, {
                        from: 1.95,
                        to: 2,
                        color: "#c00",
                        opacity: 0.8
                    }]
                },
                categoryAxis: {
                    field: "usuario",
                    labels: {
                        //rotation: -90
                    },
                    majorGridLines: {
                        visible: true
                    }
                },
                tooltip: {
                    visible: true,
                    shared: true
                    //template: "#= series.name #: #= value #"
                }
            });
        }

        $(document).ready(crearGraficoReportes);
        $(document).bind("kendo:skinChange", crearGraficoReportes);

}